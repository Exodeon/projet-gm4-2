from InterfaceTerminal import InterfaceTerminal
from Menu import Menu
from Template import Template

#choix de l'interface à tester :
interface = InterfaceTerminal()


print("TEST : menu initial : \n")
res = Menu.menu_initial(interface)
print("\n")
print("réponse : ", res)

print("\nTEST : menu fin de partie : \n")
res = Menu.menu_fin_partie(interface)
print("\n")
print("réponse : ", res)

print("\nTEST : question \"vous aimez les cartes ?\"\n")
res = interface.poser_question("vous aimez les cartes ?")
print("\n")
print("réponse : ", res)

print("\nTEST : affichage de la réponse \"Billy\"  \n")
res = interface.afficher_resultat("Billy")
print("\n")
print("réponse : ", res)


print("\nTEST : formulaire \n")
res = interface.formulaire(Template())
print("\n")
print("réponse :")
print("nom : ", res.get_nom())
print("naissance : ", res.get_date_naissance())
print("sexe : ", res.get_sexe())
print("taille : ", res.get_taille())
print("yeux : ", res.get_couleur_yeux())
print("autre caracteristiques :")
for carac in res.get_caracteristiques_vraies():
    print("  ",carac.get_nom() ," => ", carac.get_valeur())
