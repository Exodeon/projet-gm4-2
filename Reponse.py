from enum import Enum

class Reponse(Enum):
    OUI = 1
    NON = 2
    INCONNUE = 3