from Taille import Taille
from CouleurYeux import CouleurYeux
from Sexe import Sexe
from Genre import Genre


class Conversion:
    @staticmethod
    def taille_texte_en_enum(taille_texte):
        correspondances_taille = {
            "-150": Taille.T_150,
            "160-169": Taille.T160_169,
            "170-179": Taille.T170_179,
            "180-189": Taille.T180_189,
            "190-199": Taille.T190_199,
            "200-": Taille.T200_
        }

        if taille_texte in correspondances_taille.keys():
            return correspondances_taille[taille_texte]
        else:
            return Taille.INCONNUE

    @staticmethod
    def taille_enum_en_texte(taille_enum):
        correspondances_taille = {
            Taille.T_150: "-150",
            Taille.T160_169: "160-169",
            Taille.T170_179: "170-179",
            Taille.T180_189: "180-189",
            Taille.T190_199: "190-199",
            Taille.T200_: "200-"
        }

        if taille_enum in correspondances_taille.keys():
            return correspondances_taille[taille_enum]
        else:
            return ""

    @staticmethod
    def couleur_yeux_texte_en_enum(couleur_yeux_texte):
        correspondances_couleur_yeux = {
            "bleu": CouleurYeux.BLEU,
            "marron": CouleurYeux.MARRON,
            "vert": CouleurYeux.VERT,
            "gris": CouleurYeux.GRIS,
            "vairon": CouleurYeux.VAIRON
        }
        if couleur_yeux_texte in correspondances_couleur_yeux.keys():
            return correspondances_couleur_yeux[couleur_yeux_texte]
        else:
            return CouleurYeux.INCONNU

    @staticmethod
    def couleur_yeux_enum_en_texte(couleur_yeux_enum):
        correspondances_couleur_yeux = {
            CouleurYeux.BLEU: "bleu",
            CouleurYeux.MARRON: "marron",
            CouleurYeux.VERT: "vert",
            CouleurYeux.GRIS: "gris",
            CouleurYeux.VAIRON: "vairon"
        }
        if couleur_yeux_enum in correspondances_couleur_yeux.keys():
            return correspondances_couleur_yeux[couleur_yeux_enum]
        else:
            return ""

    @staticmethod
    def sexe_texte_en_enum(sexe_texte):
        correspondances_sexe = {
            "homme": Sexe.HOMME,
            "femme": Sexe.FEMME
        }
        if sexe_texte in correspondances_sexe.keys():
            return correspondances_sexe[sexe_texte]
        else:
            return Sexe.INCONNU

    @staticmethod
    def sexe_enum_en_texte(sexe_enum):
        correspondances_sexe = {
            Sexe.HOMME: "homme",
            Sexe.FEMME: "femme"
        }
        if sexe_enum in correspondances_sexe.keys():
            return correspondances_sexe[sexe_enum]
        else:
            return ""

    @staticmethod
    def genre_texte_en_enum(genre_texte):
        correspondances_genre = {
            "masculin": Genre.MASCULIN,
            "féminin": Genre.FEMININ
        }
        if genre_texte in correspondances_genre.keys():
            return correspondances_genre[genre_texte]
        else:
            return Genre.INCONNU

    @staticmethod
    def genre_enum_en_texte(genre_enum):
        correspondances_genre = {
            Genre.MASCULIN: "masculin",
            Genre.FEMININ: "féminin"
        }
        if genre_enum in correspondances_genre.keys():
            return correspondances_genre[genre_enum]
        else:
            return ""

    @staticmethod
    def inverser_sexe(sexe):
        if sexe == Sexe.HOMME:
            return Sexe.FEMME
        elif sexe == Sexe.FEMME:
            return Sexe.HOMME
        else:
            return Sexe.INCONNU
