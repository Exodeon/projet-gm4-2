import pymysql.cursors
import pymysql

# Connect to the database
connection = pymysql.connect(host='gm6.insa-rouen.fr',
                             user='nflambard',
                             password='114939',
                             db='nflambard',
                             charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        # Exemple
        #sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"

        # liste personnages
        print("Liste des personnages")
        #sql = "SELECT * FROM `Personnage` WHERE `Nom`=\"toto\""
        sql = "SELECT * FROM `Personnage`"
        cursor.execute(sql, ())
        result = cursor.fetchall()
        for row in result:
            print(row)

        print("Attributs de Jean-paul")
        sql = "SELECT nomRelation, caracteristique FROM `Relation` WHERE `idPersonnage`=(SELECT idPersonnage from Personnage WHERE `nomPersonnage`=\"Jean-paul\")"
        cursor.execute(sql, ())
        result = cursor.fetchall()
        for row in result:
            print(row)

        print("Liste des métiers de la base de données")
        sql = "SELECT `caracteristique` FROM `Relation` WHERE `nomRelation`=\"métier\""
        cursor.execute(sql, ())
        result = cursor.fetchall()
        for row in result:
            print(row["caracteristique"])
finally:
    connection.close()
