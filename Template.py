from Sexe import Sexe
from CouleurYeux import CouleurYeux
from Taille import Taille
from Personnage import Personnage
from Caracteristique import Caracteristique
from datetime import date
import time


class Template(Personnage):
    def __init__(self, nom = "inconnu", date_naissance = "", sexe = Sexe.INCONNU, taille = Taille.INCONNUE,
                couleur_yeux = CouleurYeux.INCONNU, est_vrai_nom = None, caracteristiques_vraies = None, caracteristiques_fausses = None,
                date_min = "", date_max = ""):
        super().__init__(nom, date_naissance, sexe, taille, couleur_yeux, est_vrai_nom)
        self.date_min = date_min
        self.date_max = date_max

        if caracteristiques_vraies is None:
            self.caracteristiques_vraies = []
        else:
            self.caracteristiques_vraies = caracteristiques_vraies

        if caracteristiques_fausses is None:
            self.caracteristiques_fausses = []
        else:
            self.caracteristiques_fausses = caracteristiques_fausses

    # getteur
    def get_caracteristiques_vraies(self):
        return self.caracteristiques_vraies

    def get_caracteristiques_fausses(self):
        return self.caracteristiques_fausses

    def get_date_min(self):
        return self.date_min

    def get_date_max(self):
        return self.date_max

    # setteur
    def set_date_min(self, date_min):
        self.date_min = date_min

    def set_date_max(self, date_max):
        self.date_max = date_max

    def set_caracteristiques_vraies(self, caracteristiques_vraies):
        self.caracteristiques_vraies = caracteristiques_vraies

    def set_caracteristiques_fausses(self, caracteristiques_fausses):
        self.caracteristiques_fausses = caracteristiques_fausses
