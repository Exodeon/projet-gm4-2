from Interface import Interface
from Reponse import Reponse
from Sexe import Sexe
from Taille import Taille
from CouleurYeux import CouleurYeux
from Genre import Genre
from Caracteristique import Caracteristique
from TypeCaracteristique import TypeCaracteristique
from Question import Question
from datetime import date

class InterfaceTerminal(Interface):
    def __init__(self):
        pass

    # entrée -> initial : booleen
    # sortie -> Reponse
    def afficher_menu(self, initial):
        if initial:
            print ("Bienvenue! Voulez-vous jouer? [o/n]")
        else :
            print("Voulez-vous rejouer ? [o/n]")
        choix = input().lower()
        if 'o' in choix:
            print("Parfait, allons-y :")
            return True
        else:
            print("Au-revoir et à bientôt !")
            return False

    # entrée -> question : string
    # sortie -> Reponse
    def poser_question(self, question):
        print(question)
        print("o : oui")
        print("n : non")
        print("? : je ne sais pas")
        choix = input().lower()
        if 'o' in choix:
            return Reponse.OUI
        elif 'n' in choix:
            return Reponse.NON
        else:
            return Reponse.INCONNUE


    # entrée -> nom : string
    # sortie -> booleen
    def afficher_resultat(self, nom):
        print("Le personnage est ", nom,". C'est ça ? [o/n]", sep = "")
        choix = input().lower()
        if 'o' in choix:
            print("Aucune limite à mon pouvoir !")
            return False
        else:
            print("Zut! Voulez-vous enregistrer votre personnage ? [o/n] ")
            choix = input().lower()
            return ('o' in choix)

    # entrée -> template : Template
    # sortie -> Template
    def formulaire(self, template, liste_type_caracteristique):
        print("\nFormulaire d'ajout de Personnage : \n")
        print("nom : ", template.get_nom(), "voulez vous le changer ? [o/n]")
        choix = input().lower()
        if 'o' in choix:
            print ("Entrez le nouveau nom : ")
            template.set_nom(input())

        print("naissance : ", template.get_date_naissance(), " voulez vous la changer ? [o/n]")
        choix = input().lower()
        if 'o' in choix:
            print("Entrez la nouvelle date de naissance : ")
            print("Jour :")
            jour = int(input())
            print("Mois :")
            mois = int(input())
            print("Année :")
            annee = int(input())
            template.set_date_naissance(date(annee, mois, jour))


        print("sexe : ", template.get_sexe(), " voulez vous le changer ? [o/n]")
        choix = input().lower()
        if 'o' in choix:
            print("Entrez le nouveau sexe : \n 1: homme \n 2: femme")
            choix = input()
            if (choix == "1"):
                template.set_sexe(Sexe.HOMME)
            else :
                template.set_sexe(Sexe.FEMME)

        print("taille : ", template.get_taille(), " voulez vous la changer ? [o/n]")
        choix = input().lower()
        if 'o' in choix:
            print("Entrez la nouvelle taille :")
            print(" 1: -150cm \n 2: 150-159cm \n 3: 160-169cm \n 4: 170-179cm")
            print(" 5: 180-189cm \n 6: 190-200cm \n 7: +200cm")
            choix = input()
            if (choix == "1"):
                template.set_taille(Taille.T_150)
            elif (choix == "2"):
                template.set_taille(Taille.T150_159)
            elif (choix == "3"):
                template.set_taille(Taille.T160_169)
            elif (choix == "4"):
                template.set_taille(Taille.T170_179)
            elif (choix == "5"):
                template.set_taille(Taille.T180_189)
            elif (choix == "6"):
                template.set_taille(Taille.T190_199)
            else :
                template.set_taille(Taille.T200_)

        print("couleur des yeux : ", template.get_couleur_yeux(), " voulez vous la changer ? [o/n]")
        choix = input().lower()
        if 'o' in choix:
            print ("Entrez la nouvelle couleur des yeux : ")
            print(" 1: bleu \n 2: marron \n 3: vert \n 4: gris")
            print(" 5: vairon \n 6: inconnue")
            choix = input()
            if (choix == "1"):
                template.set_couleur_yeux(CouleurYeux.BLEU)
            elif (choix == "2"):
                template.set_couleur_yeux(CouleurYeux.MARRON)
            elif (choix == "3"):
                template.set_couleur_yeux(CouleurYeux.VERT)
            elif (choix == "4"):
                template.set_couleur_yeux(CouleurYeux.GRIS)
            elif (choix == "5"):
                template.set_couleur_yeux(CouleurYeux.VAIRON)
            else :
                template.set_couleur_yeux(CouleurYeux.INCONNU)

        if template.get_est_vrai_nom() == True:
            reponse_vrai_nom = "oui"
        elif template.get_est_vrai_nom() == False:
            reponse_vrai_nom = "non"
        else:
            reponse_vrai_nom = "inconnu"

        print("Est-ce son vrai nom : ", reponse_vrai_nom , " Voulez vous changer cette réponse ? [o/n]")
        choix = input().lower()
        if 'o' in choix:
            print("Entrez la nouvelle réponse : \n 0: non \n 1: oui")
            template.set_est_vrai_nom(bool(int(input())))

        nouveaux_types_car = []     # Initialisation de la liste des nouveaux types de caracteristique
        print("liste des autres caracteristiques :", template.get_caracteristiques_vraies())
        print("Voulez-vous en ajouter d'autre ? [o/n]")
        choix = input()
        while ('o' in choix) :
            print("Type de caractéristiques déjà existants (il est possible d'en créer des nouveaux) :", liste_type_caracteristique)  # Affichage des types de caracteristique déjà existants
            print("nom de la nouvelle caractéristique :")   # Remplssage des champs de la nouvelle caractéristique
            nom_car = input()
            print("valeur associée : ")
            valeur_car = input()
            print("Est-ce un nouveau type de caractéristique ? [o/n]")
            choix = input()
            if 'o' in choix:    # Si c'est un nouveau type de caracteristique, il faut le créer
                print("Choisir un type de question possible :")
                type_car = TypeCaracteristique(nom_car, 1, Genre.INCONNU)
                caracteristique = Caracteristique(valeur_car, type_car, None)
                question = Question(caracteristique)    # Choix pour que la question soit bien posée
                print("1 : " + question.construire_question(template.get_sexe()) + " (Genre de la caractéristique à préciser)")
                question.get_caracteristique().get_type_caracteristique().set_type_question(2)
                print("2 : " + question.construire_question(template.get_sexe()))
                question.get_caracteristique().get_type_caracteristique().set_type_question(3)
                print("3 : " + question.construire_question(template.get_sexe()) + " (Verbe à préciser)")
                type_qu = int(input())
                question.get_caracteristique().get_type_caracteristique().set_type_question(type_qu)
                if type_qu == 1:    # Remplissage du genre de la caractéristique si besoin
                    print("Quel est le genre de la caractéristique ? \n 1: féminin \n 2: masculin")
                    genre = input()
                    if (choix == "1"):
                        caracteristique.get_type_caracteristique().set_genre(Genre.FEMININ)
                    else :
                        caracteristique.get_type_caracteristique().set_genre(Genre.MASCULIN)
                elif type_qu == 3:  # Remplissage du verbe associé à la caractéristique si besoin
                    print("Entrez le verbe conjugué, sous la forme nécessaire pour la question :")
                    verbe = input()
                    caracteristique.set_verbe(verbe)
                nouveaux_types_car.append(caracteristique.get_type_caracteristique())
                liste_type_caracteristique.append(caracteristique.get_type_caracteristique())

            else:   # Si c'est un type de caractéristique déjà existant, on le récupère dans la liste
                type_car = next(element for element in liste_type_caracteristique if element.get_nom() == nom_car)
                caracteristique = Caracteristique(valeur_car, type_car, None)
                if type_car.get_type_question() == 3:   # Remplissage du verbe associé à la caractéristique si besoin
                    question = Question(caracteristique)
                    print("La question sera sous la forme : \n" + question.construire_question(template.get_sexe()))
                    print("Quel verbe voulez-vous ajouter dans la question ? (entrer le verbe déjà conjugué)")
                    verbe = input()
                    caracteristique.set_verbe(verbe)

            template.get_caracteristiques_vraies().append(caracteristique)
            print("Voulez-vous en ajouter d'autres ? [o/n] \n (Caractéristique déjà existantes : ", template.get_caracteristiques_vraies(), ")")
            choix = input()


        return template, nouveaux_types_car
        # On retourne le template complété et la liste des nouveaux types de caracteristique ajoutés

    def afficher_manque_info(self):
        print("Vous ne semblez pas savoir grand chose sur ce personnage...")
        print("Je ne dispose pas d'asssez d'information pour le trouver")
