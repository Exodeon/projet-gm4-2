from BaseDeDonnees import BaseDeDonnees
from Template import Template
from Sexe import Sexe
from CouleurYeux import CouleurYeux
from Taille import Taille
from Genre import Genre
from Caracteristique import Caracteristique
from TypeCaracteristique import TypeCaracteristique
from datetime import date

bd = BaseDeDonnees()
template = Template(sexe=Sexe.HOMME)
template.set_couleur_yeux(CouleurYeux.BLEU)
template.set_date_max(date(2000, 1, 1))
template.set_date_min(date(1950, 1, 1))
print("Hommes aux yeux bleus nés entre 1950 et 2000 :")
print(bd.liste_perso(template))

print("Personnes qui mesurent entre 180 et 189 cm :")
template_b = Template(taille=Taille.T180_189)
print(bd.liste_perso(template_b))

print("Test caractéristiques supplémentaires :")
template_caracteristiques = Template(caracteristiques_vraies=[Caracteristique(valeur="brun", type_car=TypeCaracteristique(nom="couleur cheveux", type_qu=1, genre=Genre.FEMININ), verbe="")],
                          caracteristiques_fausses=[Caracteristique(valeur="française", type_car=TypeCaracteristique(nom="nationalité", type_qu=1, genre=Genre.FEMININ), verbe=""),
                                                    Caracteristique(valeur="chanteur", type_car=TypeCaracteristique(nom="métier", type_qu=1, genre=Genre.MASCULIN), verbe=""),
                                                    Caracteristique(valeur="britannique", type_car=TypeCaracteristique(nom="nationalité", type_qu=1, genre=Genre.FEMININ), verbe="")])
#template_caracteristiques = Template(caracteristiques_vraies=[Caracteristique("couleur cheveux", "brun"), Caracteristique("nationalité", "britannique"), Caracteristique("métier", "écrivain")])
print(bd.liste_perso(template_caracteristiques))

print("Personnes avec un faux nom :")
faux_noms = Template(est_vrai_nom=False)
print(bd.liste_perso(faux_noms))

# test caracteristiques restantes
hommes_francais = Template(sexe=Sexe.HOMME, caracteristiques_vraies=[Caracteristique(valeur="française", type_car=TypeCaracteristique(nom="nationalité", type_qu=1, genre=Genre.FEMININ), verbe="")])
liste_caracteristiques, nb_persos_restants = bd.caracteristiques_restantes(hommes_francais)
# print(nb_persos_restants, "persos restants")
# for element in liste_caracteristiques:
#     print(element[0].get_nom(), ":", element[0].get_valeur())



# test insertion
# adele = Template(nom="Adele", date_naissance = date(1988, 5, 5), sexe=Sexe.FEMME, taille=Taille.T170_179, couleur_yeux=CouleurYeux.VERT, est_vrai_nom=False,
#                  caracteristiques_vraies=[Caracteristique("métier", "chanteur"), Caracteristique("métier", "compositeur"),
#                                           Caracteristique("nationalité", "britannique"), Caracteristique("couleur cheveux", "brun"), Caracteristique("couleur cheveux", "blond")])

# bd.ajouter_perso(adele)

# wagner = Template(nom="Wagner", date_naissance = date(1813, 5, 22), sexe=Sexe.HOMME, taille=Taille.T170_179, couleur_yeux=CouleurYeux.MARRON, est_vrai_nom= True,
#                     caracteristiques_vraies=[Caracteristique("métier", "compositeur"), Caracteristique("style musical", "opéra"), Caracteristique("nationalité", "allemand"),
#                                              Caracteristique("couleur cheveux", "brun")])
#
# bd.ajouter_perso(wagner)