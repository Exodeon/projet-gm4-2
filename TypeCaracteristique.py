from Genre import Genre

class TypeCaracteristique:
    def __init__(self, nom, type_qu, genre):
        self.nom = nom
        self.type_question = type_qu
        self.genre = genre

    # getteur
    def get_nom(self):
        return self.nom

    def get_type_question(self):
        return self.type_question

    def get_genre(self):
        return self.genre

    # setteur
    def set_nom(self, nom):
        self.nom = nom

    def set_type_question(self, type_qu):
        self.type_question = type_qu

    def set_genre(self, genre):
        self.genre = genre

    # redéfinition de l'égalité
    def __eq__(self, autre_type_caracteristique):
        return self.get_nom() == autre_type_caracteristique.get_nom() and self.get_type_question() == autre_type_caracteristique.get_type_question() and self.get_genre() == autre_type_caracteristique.get_genre()

    # redéfinition de l'affichage
    def __str__(self):
        return self.get_nom()
    def __repr__(self):
        return str(self)