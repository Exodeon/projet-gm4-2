import pymysql.cursors
import pymysql
import time
from datetime import date

from Personnage import Personnage
from Conversion import Conversion
from Caracteristique import Caracteristique
from TypeCaracteristique import TypeCaracteristique
from Taille import Taille
from CouleurYeux import CouleurYeux
from Sexe import Sexe


class BaseDeDonnees:
    def __init__(self, nom='nflambard', hote='gm6.insa-rouen.fr', identifiant='nflambard', mot_de_passe='114939'):
        self.nom = nom
        self.hote = hote
        self.identifiant = identifiant
        self.mot_de_passe = mot_de_passe
        self.connexion = pymysql.connect(host=hote,
                                     user=identifiant,
                                     password=mot_de_passe,
                                     db=nom,
                                     charset='utf8',
                                     cursorclass=pymysql.cursors.DictCursor)

    def get_connexion(self):
        return self.connexion

    def perso_correspondant(self, resultat_bd):
        taille = Conversion.taille_texte_en_enum(resultat_bd["taille"])
        couleur_yeux = Conversion.couleur_yeux_texte_en_enum(resultat_bd["couleurYeux"])
        sexe = Conversion.sexe_texte_en_enum(resultat_bd["sexe"])
        return Personnage(resultat_bd["nomPersonnage"],
                           resultat_bd["dateNaissance"],
                           sexe,
                           taille,
                           couleur_yeux,
                           bool(resultat_bd["estVraiNom"]))

    def type_caracteristique_correspondant(self, resultat_bd):
        return TypeCaracteristique(nom=resultat_bd["nomCaracteristique"],
                                   type_qu=resultat_bd["typeQuestion"],
                                   genre=Conversion.genre_texte_en_enum(resultat_bd["genre"]))

    def liste_perso(self, template):
        with self.get_connexion().cursor() as curseur:
            # caractéristiques statiques
            requete_personnages = "SELECT `idPersonnage` FROM `Personnage`"
            premiere_condition = True
            args = []
            if template.get_date_min() != "":
                if premiere_condition:
                    premiere_condition = False
                    requete_personnages += " WHERE"
                requete_personnages += " `dateNaissance` > %s"
                args.append(template.get_date_min())

            if template.get_date_max() != "":
                if premiere_condition:
                    premiere_condition = False
                    requete_personnages += " WHERE"
                else:
                    requete_personnages += " AND"
                requete_personnages += " `dateNaissance` < %s"
                args.append(template.get_date_max())
            if template.get_sexe() != Sexe.INCONNU:
                if premiere_condition:
                    premiere_condition = False
                    requete_personnages += " WHERE"
                else:
                    requete_personnages += " AND"
                requete_personnages += " `sexe` = %s"
                args.append(Conversion.sexe_enum_en_texte(template.get_sexe()))

            if template.get_taille() != Taille.INCONNUE:
                if premiere_condition:
                    premiere_condition = False
                    requete_personnages += " WHERE"
                else:
                    requete_personnages += " AND"
                requete_personnages += " `taille` = %s"
                args.append(Conversion.taille_enum_en_texte(template.get_taille()))

            if template.get_couleur_yeux() != CouleurYeux.INCONNU:
                if premiere_condition:
                    premiere_condition = False
                    requete_personnages += " WHERE"
                else:
                    requete_personnages += " AND"
                requete_personnages += " `couleurYeux` = %s"
                args.append(Conversion.couleur_yeux_enum_en_texte(template.get_couleur_yeux()))

            if template.get_est_vrai_nom() != None:
                if premiere_condition:
                    premiere_condition = False
                    requete_personnages += " WHERE"
                else:
                    requete_personnages += " AND"
                requete_personnages += " `estVraiNom` = %s"
                args.append(template.get_est_vrai_nom())

            requete_personnages = "SELECT idPersonnage FROM (" + requete_personnages + ") AS x"

            requete_caracteristiques_vraies = ""
            # caractéristiques supplémentaires
            for caracteristique in template.get_caracteristiques_vraies():
                if requete_caracteristiques_vraies == "":
                    requete_caracteristiques_vraies = "SELECT `idPersonnage` FROM `Caracteristique` WHERE `nomCaracteristique`=%s" \
                                                      " AND `valeur` = %s"
                else:
                    requete_caracteristiques_vraies = "SELECT `idPersonnage` FROM `Caracteristique` WHERE `nomCaracteristique`=%s" \
                                                      " AND `valeur` = %s AND `idPersonnage` IN (" + requete_caracteristiques_vraies + ")"
                args.extend([caracteristique.get_nom(), caracteristique.get_valeur()])

            if requete_caracteristiques_vraies != "":
                requete_personnages += " WHERE `idPersonnage` IN (" + requete_caracteristiques_vraies + ")"

            requete_caracteristiques_fausses = ""
            for caracteristique in template.get_caracteristiques_fausses():
                if requete_caracteristiques_fausses == "":
                    requete_caracteristiques_fausses = "SELECT `idPersonnage` FROM `Caracteristique` WHERE `nomCaracteristique`=%s" \
                                                       " AND `valeur` = %s"
                else:
                    requete_caracteristiques_fausses = "SELECT `idPersonnage` FROM `Caracteristique` WHERE `nomCaracteristique`=%s" \
                                                       " AND `valeur` = %s OR `idPersonnage` IN (" + requete_caracteristiques_fausses + ")"
                args.extend([caracteristique.get_nom(), caracteristique.get_valeur()])

            if requete_caracteristiques_fausses != "":
                if requete_caracteristiques_vraies != "":
                    requete_personnages += " AND `idPersonnage` NOT IN (" + requete_caracteristiques_fausses + ")"
                else:
                    requete_personnages += " WHERE `idPersonnage` NOT IN (" + requete_caracteristiques_fausses + ")"

            curseur.execute(requete_personnages, tuple(args))
            perso_correspondants = list(map(lambda resultat: resultat['idPersonnage'], curseur.fetchall()))

            return perso_correspondants

    def ajouter_perso(self, template, nouveaux_types_caracteristiques):
        if template.get_nom() != "inconnu" and template.get_est_vrai_nom() != None:
            with self.get_connexion().cursor() as curseur:
                sql = "INSERT INTO Personnage(nomPersonnage, sexe, couleurYeux, taille, dateNaissance, estVraiNom)" \
                      "VALUES (%s, %s, %s, %s, %s, %s)"
                curseur.execute(sql, (template.get_nom(),
                                      Conversion.sexe_enum_en_texte(template.get_sexe()),
                                      Conversion.couleur_yeux_enum_en_texte(template.get_couleur_yeux()),
                                      Conversion.taille_enum_en_texte(template.get_taille()),
                                      template.get_date_naissance(),
                                      template.get_est_vrai_nom()))

                idPersonnage = self.get_connexion().insert_id()  # on doit récupérer l'id qui a été donné à l'insertion

                # ajout des nouveaux types de caractéristiques
                if len(nouveaux_types_caracteristiques) > 0:
                    args = []
                    sql = "INSERT INTO TypeCaracteristique(nomCaracteristique, typeQuestion, genre) VALUES"
                    for type_caracteristique in nouveaux_types_caracteristiques:
                        sql += "(%s, %s, %s),"
                        args.extend([type_caracteristique.get_nom(),
                                     type_caracteristique.get_type_question(),
                                     Conversion.genre_enum_en_texte(type_caracteristique.get_genre())])

                    sql = sql[:-1]  # on retire la dernière virgule
                    curseur.execute(sql, tuple(args))

                if len(template.get_caracteristiques_vraies()) > 0:
                    args = []
                    sql = "INSERT INTO Caracteristique(nomCaracteristique, idPersonnage, valeur, verbe) VALUES"
                    for caracteristique in template.get_caracteristiques_vraies():
                        sql += "(%s, %s, %s, %s),"
                        args.extend([caracteristique.get_nom(), idPersonnage, caracteristique.get_valeur(), caracteristique.get_verbe()])

                    sql = sql[:-1]  # on retire la dernière virgule
                    curseur.execute(sql, tuple(args))



                self.get_connexion().commit()

    def caracteristiques_restantes(self, template):
        id_persos_restants = self.liste_perso(template)
        nb_persos_restants = len(id_persos_restants)

        # caractéristiques supplémentaires
        chaine_args = "(" + ','.join(['%s'] * len(id_persos_restants)) + ")"  # crée un (%s, ..., %s) avec le bon nombre de %s
        sql = "SELECT `nomCaracteristique`, `valeur`, COUNT(*) AS Nombre, `verbe` FROM `Caracteristique` WHERE idPersonnage IN" + chaine_args \
              + "GROUP BY `nomCaracteristique`, `valeur`"
        args = id_persos_restants.copy()

        # caractéristiques fixes
        if template.get_sexe() == Sexe.INCONNU:
            sql += " UNION SELECT \"sexe\" AS nomCaracteristique, `sexe` AS `valeur`, COUNT(`sexe`) AS `Nombre`, NULL AS verbe FROM `Personnage`" \
                   " WHERE idPersonnage IN" + chaine_args + "GROUP BY `sexe`"
            args.extend(id_persos_restants)

        if template.get_couleur_yeux() == CouleurYeux.INCONNU:
            sql += " UNION SELECT \"couleurYeux\" AS nomCaracteristique, `couleurYeux` AS `valeur`, COUNT(`couleurYeux`) AS `Nombre`, NULL AS verbe FROM `Personnage`" \
                   " WHERE idPersonnage IN" + chaine_args + "GROUP BY `couleurYeux`"
            args.extend(id_persos_restants)

        if template.get_taille() == Taille.INCONNUE:
            sql += " UNION SELECT \"taille\" AS nomCaracteristique, `taille` AS `valeur`, COUNT(`taille`) AS `Nombre`, NULL AS verbe FROM `Personnage` " \
                   "WHERE idPersonnage IN" + chaine_args + "GROUP BY `taille`"
            args.extend(id_persos_restants)

        if template.get_date_naissance() == "":
            sql += " UNION SELECT \"dateNaissance\" AS nomCaracteristique, `dateNaissance` AS `valeur`, COUNT(`dateNaissance`) AS `Nombre`, NULL AS verbe FROM `Personnage`" \
                   " WHERE idPersonnage IN" + chaine_args + "GROUP BY `dateNaissance`"
            args.extend(id_persos_restants)

        if template.get_est_vrai_nom() is None:
            sql += " UNION SELECT \"estVraiNom\" AS nomCaracteristique, `estVraiNom` AS `valeur`, COUNT(`estVraiNom`) AS `Nombre`, NULL AS verbe FROM `Personnage` " \
                   "WHERE idPersonnage IN" + chaine_args + "GROUP BY `estVraiNom`"
            args.extend(id_persos_restants)

        liste_caracteristiques = []
        sql = "SELECT * FROM (" + sql + ") AS x LEFT JOIN TypeCaracteristique ON x.nomCaracteristique = TypeCaracteristique.nomCaracteristique"
        with self.get_connexion().cursor() as curseur:
            curseur.execute(sql, tuple(args))
            resultat = curseur.fetchall()
            for ligne in resultat:
                nom_caracteristique = ligne["nomCaracteristique"]
                if type(ligne["valeur"]) == bytes:
                    valeur = ligne["valeur"].decode(encoding="latin-1")
                else:
                    valeur = ligne["valeur"]
                if nom_caracteristique == "dateNaissance":
                    valeur = date.fromtimestamp(time.mktime(time.strptime(valeur, "%Y-%m-%d")))
                elif nom_caracteristique == "estVraiNom":
                    valeur = bool(int(valeur))

                type_caracteristique = TypeCaracteristique(nom=nom_caracteristique,
                                                           type_qu=ligne["typeQuestion"],
                                                           genre= Conversion.genre_texte_en_enum(ligne["genre"]))
                caracteristique = Caracteristique(valeur=valeur, type_car=type_caracteristique, verbe=ligne["verbe"])
                liste_caracteristiques.append([caracteristique, ligne["Nombre"]])

        # on retire les caractéristiques que l'on connaît déjà (pour les caractéristiques supplémentaires)
        liste_caracteristiques = [element for element in liste_caracteristiques if
                                  element[0] not in template.get_caracteristiques_vraies()]
        return liste_caracteristiques, nb_persos_restants

    def obtenir_nom_perso(self, id):
        with self.get_connexion().cursor() as curseur:
            sql = "SELECT nomPersonnage FROM `Personnage` WHERE `idPersonnage`=%s"
            curseur.execute(sql, (id))
            resultat = curseur.fetchone()
            return resultat["nomPersonnage"]

    def obtenir_perso(self, nom):
        with self.get_connexion().cursor() as curseur:
            sql = "SELECT * FROM `Personnage` WHERE `nomPersonnage`=%s"
            curseur.execute(sql, (nom))
            resultat = curseur.fetchall()
            return list(map(lambda ligne: self.perso_correspondant(ligne), resultat))

    def liste_type_caracteristique(self):
        with self.get_connexion().cursor() as curseur:
            sql = "SELECT * FROM `TypeCaracteristique`"
            curseur.execute(sql)
            resultat = curseur.fetchall()
            return list(map(lambda ligne: self.type_caracteristique_correspondant(ligne), resultat))



