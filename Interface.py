
class Interface:
    def __init__(self):
        pass

    # entrée -> initial : booleen
    # sortie -> String
    def afficher_menu(self, initial):
        pass

    # entrée -> question : string
    # sortie -> Reponse
    def poser_question(self, question):
        pass

    # entrée -> nom : string
    # sortie -> Reponse
    def afficher_resultat(self, nom):
        pass

    # entrée -> template : Template
    # sortie -> Template
    def formulaire(self, template, liste_type_caracteristique):
        pass

    def afficher_manque_info(self):
        pass
