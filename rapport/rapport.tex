 \documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage[left=4cm,right=4cm,top=3.5cm,bottom=3.5cm]{geometry}
\usepackage{graphicx, float, amssymb}
\usepackage{enumitem}
\usepackage{chngcntr}
\usepackage{multicol}
\usepackage[colorlinks = true,
			linkcolor = blue]{hyperref}
%opening

\title{Rapport de projet semestriel :\\ Implémentation d’un moteur d’inférence appliqué à un jeu de devinette}
\author{Marie \textsc{Bochet}, Nathan \textsc{Flambard} et Robin \textsc{Langlois}}


\begin{document}
\thispagestyle{empty}
	
	\begin{minipage}{0.45\textwidth}
		\begin{flushleft} \large
			\includegraphics[scale=0.35]{images/logo_insa.png}
		\end{flushleft}
	\end{minipage}\\
	
		\begin{minipage}{0.45\textwidth}
		\begin{flushleft} %\large
			INSA Rouen Normandie\\
			Département Génie Mathématique
		\end{flushleft}
	\end{minipage}
	\\[4.7cm]

\begin{center}

\rule{\linewidth}{0.5mm} \\[0.4cm]
    { \Large \bfseries Rapport de projet semestriel :\\ Implémentation d’un moteur d’inférence appliqué à un jeu de devinette\\[0.4cm] }
\rule{\linewidth}{0.5mm} \\[7cm]
%\textit{Author}\\
Marie \textsc{Bochet}, Nathan \textsc{Flambard} et Robin \textsc{Langlois}
			
\end{center}

	\begin{minipage}{0.45\textwidth}
		\begin{flushleft} %\large
			\textit{Professeur référent}\\
			Julien \textsc{Saunier}
		\end{flushleft}
	\end{minipage}\\
	
\begin{center}
	\today
\end{center}


\newpage

\tableofcontents

\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

Le but de ce projet était d’implémenter un moteur d’inférence appliqué à un jeu de devinette, plus précisément de créer un programme capable de trouver à qui le joueur pense en lui posant différentes questions, sur le principe du programme Akinator. Akinator est un jeu disponible en ligne où l'utilisateur peut choisir un personnage (réel ou fictif), un objet ou un animal. 
Une fois que l'utilisateur a choisi, Akinator va tenter de le trouver en posant une série de questions auquelles l'utilisateur doit répondre par oui ou par non. Il suffit d'essayer quelques fois pour se rendre compte que c'est très efficace. Pour cela, Akinator dispose d'une base de données assez complète qui est remplie de manière collaborative par les joueurs : en effet, lorsque Akinator ne trouve pas ce à quoi l'utilisateur pense, il propose alors de renseigner ses informations pour l'ajouter à sa base.

Dans le cadre de notre projet semestriel, nous avons réalisé un programme fonctionnant sur le même principe. Naturellement, nous avons dû faire quelques concessions. Tout d'abord, nous ne chercherons à faire deviner que des personnages réels. Nous voulions également proposer la possibilité de rajouter des personnages à notre base de données. Enfin, nous avons été pleinement conscients que nous n'arriverons pas au niveau de performance d'Akinator.

\newpage
\section{Problématiques à résoudre}
La première étape de ce projet a été de déterminer les problématiques qui devraient être résolues, et qui devaient conduire à la conception du projet.\\

La première problématique a été celle du stockage des données. En effet, pour que le programme puisse deviner le personnage ciblé, il faut qu'il ait accès à un nombre conséquent d'informations sur celui-ci. Nous avons donc dû définir la manière de stocker ces informations. Les deux possibilités auquelles nous avons réfléchi sont l'utilisation de fichiers ou bien d'une base de données, et celle qui nous a finalement semblé la plus adaptée est la base de données. Cette méthode permet ainsi de stocker un nombre important de données de manière organisée et plus facile d'accès qu'avec des fichiers.\\
Le langage de programmation a dû être choisi en conséquence, et nous avons finalement opté pour le python, qui permet un accès assez simple à la base de données. C'était également l'occasion pour nous de développer une application complète avec ce langage en orienté objet, alors que nous l'avions, jusque là, utilisé en programmation impérative pour des besoins mathématiques.\\

Nous avons ensuite dû définir comment organiser les différentes informations liées au personnage. Pour cela, nous les avons séparées en deux catégories : les caractéristiques fixes et les caractéristiques optionnelles. Les caractéristiques fixes correspondent aux informations qui seront renseignées pour tous les personnages, et qui ne pourront avoir qu'une valeur. Il y en a 6 : le nom, le sexe, la date de naissance, la couleur des yeux, la taille, et savoir si le nom renseigné est le vrai nom du personnage ou non. Les autres caractéristiques, qui ne seront pas nécessairement renseignées pour tous les personnages, et qui pourront avoir plusieurs valeurs pour une seule personne, seront des caractéristiques optionnelles. Par exemple, la couleur des cheveux sera une caractéristique optionnelle, car une personne peut avoir eu plusieurs colorations différentes dans sa vie.\\

Nous nous sommes également penchés sur le problème du choix des questions à poser. Le but était d'optimiser ce choix afin de réduire au maximum le nombre de personnages toujours en lice quelque soit la réponse. Il faut donc déterminer quelle caractéristique, associée à une valeur, va partager les personnages restants en deux parties les plus équitables possibles. Pour cela, nous avons utilisé la formule suivante :
$$\min \left|\frac{\mbox{nombre d'individus ayant cette caractéristique}}{\mbox{nombre d'individus total}} - 0.5\right|$$
Ainsi, pour chaque caractéristique présente chez au moins une personne encore possible, on effectue le traitement suivant : on divise la fréquence de cette caractéristique par le nombre de personnages restant pour la normaliser, puis on soustrait 0.5 pour centrer en 0. Le minimum de la valeur absolue donne alors la caractéristique dont la fréquence sera la plus proche de la moitié du nombre de personnes restantes,
et donc celle qui permettra d'éliminer le plus de gens possible en posant la question liée à cette caractéristique. Ce procédé revient donc à une méthode de recherche dichotomique. Finalement, lorsque plusieurs caractéristiques sont équivalentes, celle sur laquelle la question est posée est choisie aléatoirement afin de rendre l'ordre des questions moins prévisible.\\
Les questions sur les dates de naissance sont à traiter à part. En effet, si on le désirait, on pourrait faire uniquement une dichotomie sur la date de naissance du personnage et le trouver de cette manière. Mais cela n'est pas tout à fait souhaitable car, d'une part, l'utilisateur connaît rarement la date de naissance précise de son personnage, et d'autre part, cela diminuerait grandement l'intérêt du jeu. Pour cela, nous avons donc choisi de poser une seule fois la question sur la date de naissance. On la pose à un moment aléatoire dans le jeu, et on fait en sorte de prendre la date de naissance médiane de l'ensemble des personnages restants. De cette façon, nous utilisons tout de même l'information de la date de naissance, sans pour autant négliger les autres. Nous n'avons pas implémenté les questions sur les dates de naissance dans la première version mais dans la deuxième.

Il faut également gérer le cas où l'utilisateur ne connaît pas la réponse à la question qu'on lui pose, et on doit donc lui donner la possibilité de répondre ``je ne sais pas''. Dans ce cas, il faudra également être vigilant à ne pas lui reposer la même question.\\

Un autre aspect auquel il a fallu réfléchir est la manière de procéder lorsque le personnage auquel pense le joueur n'est pas trouvé car il n'est pas présent dans la base. Nous aurions pu considérer que le joueur devait impérativement penser à un personnage déjà renseigné dans la base, mais cela limite beaucoup le jeu, car la base sur laquelle se base le programme est petite. Nous avons donc décidé de laisser la possibilité au joueur d'ajouter un personnage à la base si celui-ci n'est pas trouvé. Cela permet ainsi d'augmenter la taille de la base, et donc augmenter progressivement la probabilité de connaître le personnage auquel pense le joueur. Lorsqu'un joueur sera amené à ajouter un nouvel individu à la base, il faudra s'assurer que les caractéristiques fixes sont bien renseignées, et conserver les informations déterminées au cours des questions pour maximiser les informations connues à son propos.\\

Enfin, la dernière problématique est celle de la formulation des questions. Il faut en effet que les questions soient compréhensibles facilement, et aient une formulation la plus naturelle possible. Pour cela, nous avons dans un premier temps opté pour une formulation commune pour toutes les questions, du type \textit{"Son/sa <nom de la caractéristique> est-il/elle <valeur de la caractéristique> ?"}. Cette forme permet d'avoir des questions compréhensibles pour la plupart des caractéristiques.



\newpage
\section{Conception}

Afin de résoudre ces problématiques, nous sommes passés par une étape de conception afin de formaliser les solutions expliquées ci-dessus.
Voici ci-dessous le diagramme de la structure de notre base de données :
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.50]{images/diag_bd_v1.png}
  \caption{Schéma entité/association de la base de données}
\end{figure}
Après passage au schéma relationnel, nous obtenons donc 2 tables en SQL :\\
 \textbf{Personnage}(idPersonnage, nomPersonnage, sexe, couleurYeux, taille, 
 dateNaissance, estVraiNom)\\
 \textbf{Caracteristique}(idCaracteristique, nomCaracteristique, idPersonnage, valeur)\\


 
Et le diagramme des classes est le suivant :
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.30]{images/diag_classe_v1.png}
  \caption{Diagramme des classes}
\end{figure}

Ici, les classes \texttt{Personnage} et \texttt{Caracteristique} font écho aux entités correspondantes dans la base.
Pour plus de commodité, nous avons utilisé quelques énumérations : \texttt{Sexe} et \texttt{CouleurYeux}.
La classe \texttt{Template} est très importante : elle permet de représenter le personnage que nous recherchons.
Elle hérite donc de Personnage puisque le but est donc de renseigner ses attributs au fur et à mesure de la partie.
On ajoute également les listes \texttt{caracteristiques\_vraies} et \texttt{caracteristiques\_fausses} qui permettent de stocker les informations collectées sur les caractéristiques optionnelles :
on ajoutera une caractéristique liée à une question posée dans l'une ou l'autre des listes selon la réponse de l'utilisateur.
Ensuite, l'énumération \texttt{Reponse} est utilisée pour plus de commodité lors du traitement de la réponse de l'utilisateur.
La classe \texttt{Question} est constituée d'une \texttt{Caracteristique} sur laquelle poser des questions, et des méthodes pour construire la question, la poser, et traiter la réponse.
La classe \texttt{Interface} permet de regrouper toutes les méthodes pour l'affichage et l'interaction avec l'utilisateur.
Elle est très liée à la classe \texttt{Menu} qui permet de faire le lien avec la logique métier.
La classe \texttt{BaseDeDonnees} est une classe utilitaire permettant de faire les requêtes en base de données.
Enfin, la classe \texttt{Akinator} permet de gérer l'ensemble de l'application en appelant les fonctionnalités de haut niveau.

% Parler de la 1ère version de l'interface : version terminal

\newpage
\section{Evolution}

Nous avons donc implémenté une première version de notre application sur la base de la conception ci-dessus.
Cependant, nous avons noté quelques pistes d'amélioration pour faire une nouvelle version, plus proche du résultat final voulu.


\subsection{Amélioration de la formulation des questions}
Nous avons décidé d'améliorer la formulation des questions. En effet, si la formulation sur une seule structure permettait dans un premier temps d'avoir des questions compréhensibles, elles n'étaient pas du tout naturelles pour certaines caractéristiques, et même inadaptées pour les questions binaires (dont la réponse est vrai ou faux), comme par exemple "Son/sa chauve est-il/elle vrai ?", ou bien "Son/sa distinction est-il/elle un prix nobel ?".\\

Pour les caractéristiques fixes, nous avons fait du cas par cas en déterminant une formulation adaptée pour chacune des 6 caractéristiques.\\
Pour les caractéristiques optionnelles, nous avons défini 3 types de questions : 
\begin{enumerate}
\item \textit{"Son(resp. sa) <nom de la caractéristique> est-il(resp. elle) <valeur de la caractéristique> ?" }: Même structure que dans la première version des questions, qui convient pour un grand nombre de caractéristiques. Mais cette fois, on prend en compte le genre de la caractéristique pour n'écrire que \textit{Son} et \textit{il} ou \textit{Sa} et \textit{elle}, et non les deux en laissant le choix au lecteur.
\item	\textit{"Est-il/elle <nom de la caractéristique> ?"} : Structure adaptée pour les caractéristiques binaires.
\item \textit{"A-t-il/elle <verbe> <valeur de la caractéristique> ?"} : Cette structure nécessite l'ajout d'un verbe pour adapter la question à la caractéristique.
\end{enumerate} ~\\
Nous avons donc ajouté dans la base de données une table \texttt{TypeCaracteristique} qui contient le nom de la caractéristique, le type de question associé et le genre si besoin.\\
Pour les questions de type 3, le verbe ne dépend pas seulement de la caractéristique, mais aussi de la valeur qui lui est associée. Par exemple, dans le cas de la distinction, on pourra avoir \textit{"A-t-il/elle \textbf{reçu} un prix nobel ?"}, mais aussi \textit{"A-t-il/elle \textbf{été} champion olympique ?"}. Le verbe est donc stocké déjà conjugué dans la table \texttt{Caracteristique}, dans une nouvelle colonne \texttt{verbe}.\\ % Ajouter un nouveau diag de BD ? (et des classes ?)
De plus, pour les types 2 et 3, nous avons pris en compte le sexe de la personne s'il est connu, pour n'afficher que \textit{il} ou \textit{elle}. Et lorsqu'on ne connaît pas encore le sexe du personnage à deviner, la question est sous la forme \textit{"Ton personnage est-il (ou a-t-il)..."}.\\

Finalement, cela permet d'avoir des questions bien compréhensibles, et naturelles pour la plupart. On aura donc par exemple :
\begin{itemize}[label=\textbullet, font=\small]
\item Est-ce un homme ?
\item Sa taille est-elle dans l'intervalle 170-179cm ?
\item Ton personnage est-il né avant le 1 janvier 1950 ?
\item Son métier est-il acteur ?
\item Sa nationalité est-elle française ?
\item Ton personnage est-il chauve ?
\item A-t-il été vainqueur de la coupe du monde ?
\end{itemize}

\subsection{Travail sur les requêtes}
% Travail sur les requêtes : mise à jour avec les modifs ci-dessus, optimisation (pourquoi ça ramait, comment a été amélioré...)

Lorsque nous avons travaillé sur la première version, nous avons cherché à obtenir rapidement une version fonctionnelle, afin d'avoir une version de démonstration.
Mais cela se fait souvent au détriment de l'efficacité de l'application. Dans notre cas, c'est principalement au niveau des requêtes en base de données que nous avons eu des problèmes d'efficacité.
En effet, la première version fonctionnait, mais on pouvait attendre jusqu'à plusieurs secondes entre chaque question.\\
Entre chaque question, il y a principalement 2 opérations à effectuer dans la base de données :
\begin{itemize}[label=\textbullet, font=\small]
 \item récupérer la liste des personnages encore en lice, c'est-à-dire compatibles avec les informations que nous possédons déjà
 \item obtenir les caractéristiques sur lesquelles on peut poser des questions (c'est-à-dire celles qu'on ne connaît pas encore sur les personnages en lice)
\end{itemize}
Ces 2 opérations sont délicates à écrire en SQL. Dans un premier temps, nous les avons donc faites en faisant l'intersection de plusieurs requêtes.\\

Tout d'abord, pour récupérer la liste des personnages :
\begin{itemize}[label=\textbullet, font=\small]
 \item on récupérait la liste des personnages qui possédaient les caractéristiques statiques (sélection sur la table \texttt{Personnage})
 \item dans cette liste, on enlevait les personnages qui ne possédaient pas les ``caractéristiques vraies'' (les caractéristiques optionnelles que l'on sait que le personnage a)
 \item puis on retirait les personnages qui possédaient les caractéristiques fausses
\end{itemize}

On effectuait donc une requête pour le premier point, puis on itérait sur les caractéristiques vraies et fausses, et on faisait une requête pour chaque. 
Finalement, on faisait donc beaucoup de requêtes, surtout vers la fin du jeu où on a beaucoup de caractéristiques vraies et fausses.
Multiplier les requêtes augmente grandement le temps d'exécution puisqu'on multiplie les communications entre l'application et la base, il faut donc chercher à les limiter.

Pour cette opération, nous l'avons finalement écrite en une seule requête. Nous avons gardé la première étape qui était en une seule requête (sélection sur la table \texttt{Personnage}), puis nous avons concaténé à cela une requête imbriquée pour chacune des caractéristiques optionnelles, en vérifiant que le personnage (par le biais de son id) possède les caractéristiques vraies et pas les fausses.\\

Pour la deuxième opération, il faut d'abord récupérer la liste des personnages (donc effectuer l'opération précédente). 
Ensuite, il faut récupérer toutes les caractéristiques de ces personnages (fixes et optionnelles), et retirer celles que l'on connaît déjà.
Dans la première version, nous n'avions pas les caractéristiques statiques, seulement les caractéristiques optionnelles. 
Pour ces dernières, il suffit de faire une sélection sur la table \texttt{Caracteristique}.
Le problème de cette solution n'était donc pas son efficacité mais le fait qu'elle ne répondait pas complètement au problème.
Pour rajouter les caractéristiques fixes, nous avons fait des \texttt{UNION} puis rajouté chacune des caractéristiques fixes (qui sont des champs de la table \texttt{Personnage}) en concaténant plusieurs requêtes.
Finalement, il nous suffit également d'une requête pour faire cela, ce qui fait donc 2 requêtes pour cette opération puisque l'on utilise la première.\\
Lorsque nous avons rajouté la table \texttt{TypeCaracteristique}, il a fallu rajouter une jointure avec cette table à notre résultat pour récupérer le type de question et le genre de chaque caractéristique.\\

Nous avons également l'opération pour ajouter un personnage.
Il faut insérer des données potentiellement dans les 3 tables de la base de données, donc on fait jusqu'à 3 requêtes dans cette opération. A priori, le seul moyen d'améliorer cette opération serait d'envoyer les 3 requêtes en même temps à la base.
Mais nous avons pensé que cette opération était moins prioritaire à améliorer car celle-ci ne se fait qu'une seule fois à la fin du jeu, et simplement dans le cas où le personnage n'existe pas dans la base et que le joueur souhaite le rajouter.

\subsection{Interface graphique}
% Interface graphique : prise en main de Tkinter, problèmes rencontrés, captures du résultat...
Dans la première version de l'application, nous avons utilisé une interface en terminal. Cette version de l'interface posait plusieurs problèmes notamment au niveau de la lisibilité. En particulier, la possibilité d'ajouter un personnage à la base de données était assez austère pour l'utilisateur. Afin d'améliorer ses aspects, nous avons développé une version graphique de l'interface à l'aide du package Tkinter.

Durant la mise en place de cette interface graphique, nous avons rencontré plusieurs difficultés. Tout d'abord, la prise en main de Tkinter n'a pas été évidente au début. Le principal problème a été de réussir à comprendre comment placer les différents éléments au bon endroit de la fenêtre en utilisant les différents layout manager (pack et grid). Un autre problème que nous avons rencontré est que la version initiale du programme n'était pas adaptée à la programmation évènementielle utilisée par Tkinter. Nous avons donc été obligés de modifier le comportement de l'application dans le cas où l'on utilise l'interface graphique. Par exemple, la méthode \texttt{partie()} de la classe \texttt{Akinator()} n'était pas adaptée à l'utilisation d'une interface graphique. Par conséquent, dans la version graphique de notre application, les appels de méthodes qui apparaissaient auparavant dans la méthode \texttt{partie()} se font en réaction à un événement (correspondant en général à cliquer sur un bouton). En procédant ainsi, nous avons pu régler les problèmes de compatibilité entre l'interface graphique et la logique métier sans empêcher la version terminal de fonctionner. \\          

L'interface graphique que nous avons mise au point se décompose en plusieurs écrans. Nous allons maintenant présenter les principaux écrans de l'application : 

% écran de question

L'écran des questions est l'écran principal de l’application et celui sur lequel les utilisateurs passeront le plus de temps. Il s'agit de l'écran permettant de faire le lien entre les réponses de l’utilisateur aux questions qui lui sont posées et la logique métier de l'application. Il se présente ainsi : 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.27]{images/question.png}
  \caption{Écran des questions}
\end{figure}


L'écran affiche la question générée par la classe \texttt{Akinator} et propose 3 réponses possibles à l'utilisateur : "oui", "non" et "je ne sais pas". Quand l'utilisateur clique sur l'un des boutons, on transmet la réponse choisie à la classe \texttt{Akinator} et on génère une nouvelle question que l'on affiche. On procède ainsi jusqu'à ce que le personnage soit trouvé (où qu'il n'y ait plus de questions à poser). On affiche alors le résultat et on demande s'il est correct. Si ce n'est pas le cas, on demande à l'utilisateur s'il veut enregistrer le personnage auquel il pensait et on passe au formulaire d’ajout de personnage. L'affichage du résultat se fait donc sous une forme assez similaire à l'écran des questions :   

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.27]{images/resultat.png}
  \caption{Affichage du résultat}
\end{figure}

% écran formulaire

Le formulaire d’ajout de personnage est l'écran le plus complexe que nous avons eu à réaliser. Il comporte en effet beaucoup de composants de différentes natures (entrée de texte, liste déroulante, case à cocher.... ). Il a l'aspect suivant : 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.27]{images/formulaire.png}
  \caption{Formulaire d'ajout de personnage}
\end{figure}


L'écran est divisé en plusieurs sections permettant à l'utilisateur de modifier les différentes informations (comme la taille, le nom, la couleur des yeux...) du personnage qu'il veut ajouter dans la base de données. A l'arrivée sur l'écran, les informations qui sont déjà connues pour le personnage à enregistrer sont pré-remplies. Cet écran permet également d'ajouter de nouveaux types de caractéristiques. Or, nous avons vu que nous disposions de plusieurs façons de poser une question en fonction du type de caractéristique. Afin de bien orienter le choix de l'utilisateur, quand il utilise le bouton "ajouter" de la section type de caractéristique, le programme ouvre un pop-up avec le format de la question posée et demande à l'utilisateur de valider son choix. De même, comme certaines caractéristiques nécessitent de renseigner un verbe, on procède de la même manière quand l'utilisateur souhaite ajouter une nouvelle caractéristique à son personnage. Enfin, si on essaye d'enregistrer un personnage existant déjà dans la base de données, on le signale à l'utilisateur via une fenêtre d'erreur. 
 






\subsection{Mise à jour des diagrammes en conséquence}

Les différentes évolutions évoquées nous ont conduit à ces nouveaux diagrammes : 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.50]{images/diag_bd_v2.png}
  \caption{Schéma entité/association de la base de données}
\end{figure}
On a donc une nouvelle table dans la base de données MySQL :\\
 \textbf{TypeCaracteristique}(nomCaracteristique, typeQuestion, genre)\\

On a également ajouté un attribut ``verbe'' dans la table \texttt{Caracteristique}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.30]{images/diag_classe_v2.png}
	
  \caption{Diagramme des classes}
\end{figure}

Pour l'amélioration de la formulation des questions, nous avons donc rajouté la classe \texttt{TypeCaracteristique} et l'énumération \texttt{Genre}, et une méthode dans la classe \texttt{BaseDeDonnees} pour récupérer la liste des types de caractéristiques stockés.
Nous avons rajouté l'exception \texttt{NoQuestionsException} qui est levée lorsque nous n'avons plus de questions à poser et que nous ne pouvons pas trouver le personnage : cela arrive lorsqu'on répond trop de fois ``je ne sais pas''.
Nous avons rajouté la classe \texttt{Conversion} qui est une classe statique permet de faire le lien entre les chaînes de caractères stockées dans la base et nos énumérations.

\newpage
\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}
Après avoir testé notre programme, nous sommes relativement satisfait du résultat obtenu. Avec notre base de données actuelle, nous trouvons généralement le personnage en un nombre raisonnable de questions. Nous avons pu implémenter l'ajout d'un personnage, ce qui rend notre base de données extensible à l'infini.
Il n'y a pas beaucoup de temps d'attente entre chaque question, et elles sont formulées de façon assez naturelle. Nous sommes également parvenus à réaliser une interface graphique fonctionnelle et satisfaisante visuellement.

Notre projet possède plusieurs pistes d'amélioration possibles : tout d'abord, nous avons remarqué qu'il y avait des redondances dans la table \texttt{Caracteristique}, notamment lorsque des personnages ont la même caractéristique. Pour l'éviter, nous aurions pu revoir notre schéma relationnel de base de données pour mieux exploiter la relation ``avoir'' entre \texttt{Personnage} et \texttt{Caracteristique}.

Nous aurions aussi pu introduire une notion de passé et de présent dans les caractéristiques, afin de mieux faire la différence lorsqu'un personnage a une caractéristique avec plusieurs valeurs différentes. Par exemple, un personnage peut avoir eu plusieurs couleurs de cheveux dans sa vie, et ici nous ne faisons pas la différence entre celle qu'il a actuellement et celles qu'il a pu avoir dans le passé. Cela pourrait se faire en ajoutant un attribut aux caractéristiques.
Cette possibilité pose cependant plusieurs problèmes : d'une part, il faut mettre à jour la base de données assez régulièrement. D'autre part, cela peut induire en erreur car si l'on nous demande si notre personnage \textbf{a} une caractéristique qu'il \textbf{a eu} dans le passé, nous pouvons être tenté de répondre oui.

Cela nous amène donc à une dernière piste d'amélioration : il faudrait que le programme soit plus tolérant aux erreurs de l'utilisateur. Dans notre cas, une mauvaise réponse sur une caractéristique nous empêche complètement de trouver le personnage que nous cherchons. Cet aspect est bien géré dans l'Akinator original, qui peut souvent trouver notre personnage malgré une erreur, même si cela peut demander plus de temps.

\newpage
\section*{Annexe : Liste des personnages}
\addcontentsline{toc}{section}{Annexe : Liste des personnages}

\begin{multicols}{2}
    \begin{itemize}
        \item Kylian Mbappé
        \item Rihanna             
        \item Harrison Ford       
        \item Mozart              
        \item Bach                
        \item Molière             
        \item Victor Hugo         
        \item Martin Fourcade     
        \item J.K Rowling         
        \item Elon Musk           
        \item William Shakespeare 
        \item Galilée             
        \item Stephen Hawking     
        \item Marilyn Monroe      
        \item Nelson Mandela      
        \item Tony Parker         
        \item Albert Einstein     
        \item Agatha Christie     
        \item Omar Sy             
        \item Soprano             
        \item Alain Chabat        
        \item Thomas Pesquet      
        \item Marie Curie         
        \item Stéphane Bern       
        \item Lady Gaga           
        \item Gérard Depardieu    
        \item Maggie Smith        
        \item Charles de Gaulle   
        \item Angela Merkel       
        \item Nathan Flambard     
        \item Elizabeth II        
        \item Robin Langlois      
        \item Marie Bochet        
        \item Adele               
        \item Wagner              
        \item Laura Flessel       
        \item Laure Manaudou      
        \item Margaret Thatcher   
        \item Jean-Baptiste Lully 
        \item Michel Drucker

    \end{itemize}
\end{multicols}

\end{document}
}
