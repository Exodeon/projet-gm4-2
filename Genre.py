from enum import Enum

class Genre(Enum):
    MASCULIN = 1
    FEMININ = 2
    INCONNU = 3