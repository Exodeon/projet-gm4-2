from Caracteristique import Caracteristique
from TypeCaracteristique import TypeCaracteristique
from Reponse import Reponse
from Conversion import Conversion
from Genre import Genre
from Sexe import Sexe

class Question:
    def __init__(self, caracteristique):
        self.caracteristique = caracteristique

    # getteur
    def get_caracteristique(self):
        return self.caracteristique

    # setteur
    def set_caracteristique(self, caracteristique):
        self.caracteristique = caracteristique

    # méthodes
    def construire_question(self, sexe_perso):
        # Méthode construsant la syntaxe de la question à partir de la caractéristique
        nom_caracteristique = self.get_caracteristique().get_nom()
        valeur_caracteristique = self.get_caracteristique().get_valeur()
        type_question = self.get_caracteristique().get_type_caracteristique().get_type_question()

        # On construit les questions :
        # Si c'est une caractéristique fixe :
        if nom_caracteristique == "sexe":
            if valeur_caracteristique == "homme":
                question = "Est-ce un homme ?"
            else:
                question = "Est-ce une femme ?"
        elif nom_caracteristique == "taille":
            question = "Sa taille est-elle dans l'intervalle " + valeur_caracteristique + "cm ?"
        elif nom_caracteristique == "couleurYeux":
            question = "Ses yeux sont-ils "+ valeur_caracteristique + " ?"
        elif nom_caracteristique == "dateNaissance":
            if sexe_perso == Sexe.HOMME:
                question = "Est-il né avant le 1er Janvier " + str(valeur_caracteristique.year) + " ?"
            elif sexe_perso == Sexe.FEMME:
                question = "Est-elle née avant le 1er Janvier " + str(valeur_caracteristique.year) + " ?"
            else:
                question = "Ton personnage est-il né avant le 1er Janvier " + str(valeur_caracteristique.year) + " ?"
        elif nom_caracteristique == "estVraiNom":
            question = "Est-ce son vrai nom ?"
         # Si c'est un caractéristiqe non fixe :
        elif type_question == 1:
            if self.get_caracteristique().get_type_caracteristique().get_genre() == Genre.MASCULIN:
                question = "Son "+ nom_caracteristique + " est-il "+ valeur_caracteristique + " ?"
            elif self.get_caracteristique().get_type_caracteristique().get_genre() == Genre.FEMININ:
                if nom_caracteristique[0] in {'a', 'e', 'i', 'o', 'u', 'y', 'é', 'è'}:
                    question = "Son "+ nom_caracteristique + " est-elle "+ valeur_caracteristique + " ?"
                else:
                    question = "Sa "+ nom_caracteristique + " est-elle "+ valeur_caracteristique + " ?"
            else:
                question = "Son/Sa "+ nom_caracteristique + " est-il/elle "+ valeur_caracteristique + " ?"
        elif type_question == 2:
            if sexe_perso == Sexe.HOMME:
                question = "Est-il " + nom_caracteristique + " ?"
            elif sexe_perso == Sexe.FEMME:
                question = "Est-elle " + nom_caracteristique + " ?"
            else:
                question = "Ton personnage est-il " + nom_caracteristique + " ?"
        elif type_question == 3:
            if self.get_caracteristique().get_verbe() == None:
                verbe = "<verbe à ajouter>"
            else:
                verbe = self.get_caracteristique().get_verbe()
            if sexe_perso == Sexe.HOMME:
                question = "A-t-il " + verbe + " " + valeur_caracteristique + " ?"
            elif sexe_perso == Sexe.FEMME:
                question = "A-t-elle " + verbe + " " + valeur_caracteristique + " ?"
            else:
                question = "Ton personnage a-t-il " + verbe + " " + valeur_caracteristique + " ?"
        return question

    def traiter_reponse(self, reponse, template):
        # Méthode qui traite la réponse en fonction de la question posée
        if reponse == Reponse.OUI:
            if self.get_caracteristique().get_nom() == "dateNaissance":
                template.set_date_max(self.get_caracteristique().get_valeur()) # On demande tjrs si le perso est né AVANT telle date
            elif self.get_caracteristique().get_nom() == "sexe":
                template.set_sexe(Conversion.sexe_texte_en_enum(self.get_caracteristique().get_valeur()))
            elif self.get_caracteristique().get_nom() == "couleurYeux":
                template.set_couleur_yeux(Conversion.couleur_yeux_texte_en_enum(self.get_caracteristique().get_valeur()))
            elif self.get_caracteristique().get_nom() == "taille":
                template.set_taille(Conversion.taille_texte_en_enum(self.get_caracteristique().get_valeur()))
            elif self.get_caracteristique().get_nom() == "estVraiNom":
                template.set_est_vrai_nom(True)
            else:
                template.get_caracteristiques_vraies().append(self.get_caracteristique())
        elif reponse == Reponse.NON:
            if self.get_caracteristique().get_nom() == "dateNaissance":
                template.set_date_min(self.get_caracteristique().get_valeur()) # On demande tjrs si le perso est né AVANT telle date
            elif self.get_caracteristique().get_nom() == "sexe":
                template.set_sexe(Conversion.inverser_sexe(Conversion.sexe_texte_en_enum(self.get_caracteristique().get_valeur())))
            elif self.get_caracteristique().get_nom() == "estVraiNom":
                template.set_est_vrai_nom(False)
            else:
                template.get_caracteristiques_fausses().append(self.get_caracteristique())

    def poser(self, template, interface):
        reponse = interface.poser_question(self.construire_question(template.get_sexe()))
        return self.traiter_reponse(reponse, template)
