from Interface import Interface
from Akinator import Akinator
from Reponse import Reponse
from Sexe import Sexe
from Taille import Taille
from Conversion import Conversion
from CouleurYeux import CouleurYeux
from Caracteristique import Caracteristique
from Genre import Genre
from TypeCaracteristique import TypeCaracteristique
from Question import Question
from NoQuestionsException import NoQuestionsException
from datetime import date
import tkinter as tk
import tkinter.messagebox as tk_box
from tkinter import font as tkFont


from PIL import Image, ImageTk


class InterfaceGraphique(Interface, tk.Tk):
    def __init__(self):
        Interface.__init__(self)
        tk.Tk.__init__(self)
        self.__fenetre_principale = tk.Frame(master=self,bg="navajo white")
        self.__boutons = dict()
        self.__champs = dict()
        self.__entree = dict()
        self.__blocs = dict()
        self.__akinator = Akinator(interface=self)


        # variables pour le formulaire
        self.liste_type_carac = []
        self.nouveaux_types_car = []
        self.entree_nom = tk.StringVar()
        self.entree_vrai_nom = tk.BooleanVar()
        self.entree_jour_nais = tk.IntVar()
        self.entree_mois_nais = tk.IntVar()
        self.entree_annee_nais = tk.IntVar()
        self.entree_taille = tk.StringVar()
        self.entree_sexe = tk.StringVar()
        self.entree_yeux = tk.StringVar()
        self.entree_valeur_carac = tk.StringVar()
        self.entree_verbe_carac = tk.StringVar()
        self.entree_type_question = tk.IntVar()
        self.entree_nom_carac = tk.StringVar()

        self.__fenetre_principale.grid()
        self.__fenetre_boutons = tk.Frame(bg="navajo white")
        self.__police_accueil = tkFont.Font(family="Utopia", size=40, weight=tkFont.BOLD)
        self.__police_titre = tkFont.Font(family="Utopia", size=16, weight=tkFont.BOLD)
        self.__police = tkFont.Font(family="Utopia", size=12, weight=tkFont.BOLD)
        self.__police_formulaire = tkFont.Font(family="Utopia", size=10, weight=tkFont.BOLD)

        self.geometry("925x800")
        self.title("Akinator")
        self.configure(bg = "navajo white")

    def __get_boutons(self):
        return self.__boutons

    def __get_champs(self):
        return self.__champs

    def __get_fenetre_principale(self):
        return self.__fenetre_principale

    def __get_fenetre_boutons(self):
        return self.__fenetre_boutons

    def __get_police(self):
        return self.__police

    def __get_police_titre(self):
        return self.__police_titre

    def __get_police_accueil(self):
        return self.__police_accueil

    def __get_police_formulaire(self):
        return self.__police_formulaire

    def __get_akinator(self):
        return self.__akinator

    def __get_entree(self):
        return self.__entree

    def __get_blocs(self):
        return self.__blocs

    def nettoyer_fenetre(self):
        for bouton in self.__get_boutons().values():
            bouton.destroy()
        self.__get_boutons().clear()
        for champ in self.__get_champs().values():
            champ.destroy()
        self.__get_champs().clear()
        for entree in self.__get_entree().values():
            entree.destroy()
        self.__get_entree().clear()
        for bloc in self.__get_blocs().values():
            bloc.destroy()
        self.__get_blocs().clear()
        self.__get_fenetre_boutons().destroy()
        self.configure(bg = "navajo white")


    def lancer_partie(self):
        self.__get_akinator().reinitialiser()
        if self.__get_akinator().est_trouve() :
            nom = self.__get_akinator().get_base().obtenir_nom_perso(self.__get_akinator().get_base().liste_perso(self.__get_akinator().get_template())[0])
            self.afficher_resultat(nom)
        elif self.__get_akinator().perdu() :
            self.formulaire(self.__get_akinator().get_template())
        else :
            try:
                self.poser_question(self.__get_akinator().trouver_question())
            except NoQuestionsException:
                self.afficher_manque_info()


    # entrée -> initial : booleen
    # sortie -> booleen
    def afficher_menu(self, initial, victoire):
        self.nettoyer_fenetre()

        # Configuration du gestionnaire de grille

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1, minsize = 350)
        self.rowconfigure(1, weight=1)
        self.columnconfigure(1, weight=1)

        self.__fenetre_boutons = tk.Frame(bg="navajo white")
        self.__get_fenetre_boutons().grid(row=0, column=0)
        self.__get_boutons()["jouer"] = tk.Button(self.__get_fenetre_boutons())
        if initial :
            load = Image.open("akinator.png")
            render = ImageTk.PhotoImage(load)
            img = tk.Label(self, image=render)
            img.image = render
            img.configure(bg = "navajo white")
            img.grid(column = 1, row = 0, pady = 10)
            self.__get_boutons()["jouer"]["text"] = "Jouer"
        else :
            self.__get_boutons()["jouer"]["text"] = "Rejouer"

        self.__get_champs()["titre"] = tk.Label(self.__get_fenetre_boutons(), text="Akinator", wraplength = 300)
        self.__get_champs()["titre"].config(bg="navajo white", fg="brown4")
        self.__get_champs()["titre"]["font"] = self.__get_police_accueil()
        self.__get_champs()["titre"].grid(pady = 50)

        if victoire:
            self.__get_champs()["gagne"] = tk.Label(self.__get_fenetre_boutons(), text="Aucune limite à mon pouvoir !", wraplength = 300)
            self.__get_champs()["gagne"].config(bg="navajo white", fg="brown4")
            self.__get_champs()["gagne"]["font"] = self.__get_police_titre()
            self.__get_champs()["gagne"].grid(pady = 10, ipadx = 8, ipady = 8)

        self.__get_boutons()["jouer"]["command"] = self.lancer_partie
        self.__get_boutons()["jouer"].config(bg = "burlywood2", fg="brown4")
        self.__get_boutons()["jouer"]["font"] = self.__get_police_titre()
        self.__get_boutons()["jouer"].grid(pady = 15, ipadx = 8, ipady = 8)
        self.__get_boutons()["quitter"] = tk.Button(self.__get_fenetre_boutons())
        self.__get_boutons()["quitter"]["text"] = "Quitter"
        self.__get_boutons()["quitter"]["command"] = self.destroy
        self.__get_boutons()["quitter"].config(bg = "burlywood2", fg="brown4")
        self.__get_boutons()["quitter"]["font"] = self.__get_police_titre()
        self.__get_boutons()["quitter"].grid(pady = 15, ipadx = 8, ipady = 8)


    def gerer_reponse(self, reponse, question) :
        question.traiter_reponse(reponse, self.__get_akinator().get_template())
        if self.__get_akinator().est_trouve() :
            nom = self.__get_akinator().get_base().obtenir_nom_perso(self.__get_akinator().get_base().liste_perso(self.__get_akinator().get_template())[0])
            self.afficher_resultat(nom)
        elif self.__get_akinator().perdu() :
            self.formulaire(self.__get_akinator().get_template())
        else :
            try:
                self.poser_question(self.__get_akinator().trouver_question())
            except NoQuestionsException:
                self.afficher_manque_info()





    # entrée -> question : string
    # sortie -> Reponse
    def poser_question(self,question):
        self.__get_akinator().get_questions().append(question)

        self.nettoyer_fenetre()
        self.__fenetre_boutons = tk.Frame(bg="navajo white")
        self.__get_fenetre_boutons().grid(row=0, column=0)

        self.__get_champs()["question"] = tk.Label(self.__get_fenetre_boutons(), text=question.construire_question(self.__get_akinator().get_template().get_sexe()), wraplength = 300)
        self.__get_champs()["question"].config(bg="navajo white", fg="brown4")
        self.__get_champs()["question"]["font"] = self.__get_police_titre()
        self.__get_champs()["question"].grid(pady = 15, ipadx = 8, ipady = 8)

        self.__get_boutons()["oui"] = tk.Button(self.__get_fenetre_boutons())
        self.__get_boutons()["oui"]["text"] = "Oui"
        self.__get_boutons()["oui"]["command"] = lambda : self.gerer_reponse(Reponse.OUI, question)
        self.__get_boutons()["oui"].config(bg="burlywood2", fg="brown4")
        self.__get_boutons()["oui"]["font"] = self.__get_police()
        self.__get_boutons()["oui"].grid(pady = 10, ipadx = 8, ipady = 8)

        self.__get_boutons()["non"] = tk.Button(self.__get_fenetre_boutons())
        self.__get_boutons()["non"]["text"] = "Non"
        self.__get_boutons()["non"]["command"] = lambda : self.gerer_reponse(Reponse.NON, question)
        self.__get_boutons()["non"].config(bg="burlywood2", fg="brown4")
        self.__get_boutons()["non"]["font"] = self.__get_police()
        self.__get_boutons()["non"].grid(pady = 10, ipadx = 8, ipady = 8)

        self.__get_boutons()["neSaisPas"] = tk.Button(self.__get_fenetre_boutons())
        self.__get_boutons()["neSaisPas"]["text"] = "Je ne sais pas"
        self.__get_boutons()["neSaisPas"]["command"] = lambda  : self.gerer_reponse(Reponse.INCONNUE, question)
        self.__get_boutons()["neSaisPas"].config(bg="burlywood2", fg="brown4")
        self.__get_boutons()["neSaisPas"]["font"] = self.__get_police()
        self.__get_boutons()["neSaisPas"].grid(pady = 10, ipadx = 8, ipady = 8)



    # entrée -> nom : string
    # sortie -> Reponse
    def afficher_resultat(self, nom):
        self.nettoyer_fenetre()
        self.__fenetre_boutons = tk.Frame(bg="navajo white")
        self.__get_fenetre_boutons().grid(row=0, column=0)

        self.__get_champs()["résultat"] = tk.Label(self.__get_fenetre_boutons(), text="Votre personnage est " + nom + ". C'est ça ?", wraplength = 300)
        self.__get_champs()["résultat"].config(bg="navajo white", fg="brown4")
        self.__get_champs()["résultat"]["font"] = self.__get_police_titre()
        self.__get_champs()["résultat"].grid()

        self.__get_boutons()["oui"] = tk.Button(self.__get_fenetre_boutons())
        self.__get_boutons()["oui"]["text"] = "Oui"
        self.__get_boutons()["oui"]["command"] = lambda : self.afficher_menu(initial=False, victoire=True)
        self.__get_boutons()["oui"].config(bg="burlywood2", fg="brown4")
        self.__get_boutons()["oui"]["font"] = self.__get_police()
        self.__get_boutons()["oui"].grid(pady = 10, ipadx = 8, ipady = 8)

        self.__get_boutons()["non"] = tk.Button(self.__get_fenetre_boutons())
        self.__get_boutons()["non"]["text"] = "Non"
        self.__get_boutons()["non"]["command"] = lambda : self.question_formulaire()
        self.__get_boutons()["non"].config(bg="burlywood2", fg="brown4")
        self.__get_boutons()["non"]["font"] = self.__get_police()
        self.__get_boutons()["non"].grid(pady = 10, ipadx = 8, ipady = 8)

    def question_formulaire(self):
        if tk_box.askyesno("Ajout du personnage ?", "Voulez-vous enregistrer votre personnage ?"):
            self.formulaire(self.__get_akinator().get_template())
        else:
            self.afficher_menu(initial=False, victoire=False)


    # entrée -> template : Template
    # sortie -> Template
    def formulaire(self, template):
        self.nettoyer_fenetre()

        self.__get_champs()["titre"] = tk.Label(self.__get_fenetre_principale(), text="Ajouter un personnage",bg="navajo white")
        self.__get_champs()["titre"].pack()

        # nom
        self.__get_blocs()["nom"] = tk.LabelFrame(self.__get_fenetre_principale(),text="Nom", padx=20, pady=2,bg="navajo white")

        self.entree_nom.set(template.get_nom())
        self.__get_entree()["nom"] = tk.Entry(self.__get_blocs()["nom"], textvariable=self.entree_nom)
        self.__get_entree()["nom"].pack()

        # vrai nom :
        if template.get_est_vrai_nom() is not None :
            self.entree_vrai_nom.set(template.get_est_vrai_nom())
        else :
            self.entree_vrai_nom.set(True)

        self.__get_entree()["vraiNom"] = tk.Checkbutton(self.__get_blocs()["nom"],text="vrai nom",variable=self.entree_vrai_nom,bg="navajo white")
        self.__get_entree()["vraiNom"].pack()

        self.__get_blocs()["nom"].pack()

        # date de naissance
        self.__get_blocs()["naissance"] = tk.LabelFrame(self.__get_fenetre_principale(),text="Date de Naissance", padx=20, pady=2,bg="navajo white")
        self.__get_blocs()["naissance"].rowconfigure(0, weight=1)
        self.__get_blocs()["naissance"].columnconfigure(0, weight=1)
        self.__get_blocs()["naissance"].rowconfigure(1, weight=1)
        self.__get_blocs()["naissance"].columnconfigure(1, weight=1)
        self.__get_blocs()["naissance"].columnconfigure(2, weight=1)
        #jour
        self.__get_champs()["Jour"] = tk.Label(self.__get_blocs()["naissance"], text="Jour",bg="navajo white")
        self.__get_champs()["Jour"].grid(row=0, column=0)
        self.entree_jour_nais.set(1)
        self.__get_entree()["naissanceJour"] = tk.Spinbox(self.__get_blocs()["naissance"], from_=1, to=31, width=4, textvariable=self.entree_jour_nais)
        self.__get_entree()["naissanceJour"].grid(row=1, column=0)
        #mois
        self.__get_champs()["Mois"] = tk.Label(self.__get_blocs()["naissance"], text="Mois",bg="navajo white")
        self.__get_champs()["Mois"].grid(row=0, column=1)
        self.entree_mois_nais.set(1)
        self.__get_entree()["naissanceMois"] = tk.Spinbox(self.__get_blocs()["naissance"], from_=1, to=12, width=4, textvariable=self.entree_mois_nais)
        self.__get_entree()["naissanceMois"].grid(row=1, column=1)
        #année
        self.__get_champs()["Année"] = tk.Label(self.__get_blocs()["naissance"], text="Année",bg="navajo white")
        self.__get_champs()["Année"].grid(row=0, column=2)
        self.entree_annee_nais.set(1)
        self.__get_entree()["naissanceAnnée"] = tk.Spinbox(self.__get_blocs()["naissance"], from_=0, to=2020, width=6, textvariable=self.entree_annee_nais)
        self.__get_entree()["naissanceAnnée"].grid(row=1, column=2)

        self.__get_blocs()["naissance"].pack()

        #bloc taille + sexe + couleurs des yeux
        self.__get_blocs()["taille+sexe+yeux"] = tk.Frame(master=self.__get_fenetre_principale(),bg="navajo white")

        # taille

        self.__get_blocs()["taille"] = tk.LabelFrame(self.__get_blocs()["taille+sexe+yeux"],text="Échelle de taille", padx=20, pady=21,bg="navajo white")
        self.entree_taille.set(Conversion.taille_enum_en_texte(template.get_taille()))
        self.__get_entree()["taille150"] =  tk.Radiobutton(self.__get_blocs()["taille"], text="-150cm", variable=self.entree_taille, value=Conversion.taille_enum_en_texte(Taille.T_150),bg="navajo white")
        self.__get_entree()["taille150"].pack()
        self.__get_entree()["taille159"] =  tk.Radiobutton(self.__get_blocs()["taille"], text="150-159cm", variable=self.entree_taille, value=Conversion.taille_enum_en_texte(Taille.T150_159),bg="navajo white")
        self.__get_entree()["taille159"].pack()
        self.__get_entree()["taille169"] =  tk.Radiobutton(self.__get_blocs()["taille"], text="160-169cm", variable=self.entree_taille, value=Conversion.taille_enum_en_texte(Taille.T160_169),bg="navajo white")
        self.__get_entree()["taille169"].pack()
        self.__get_entree()["taille179"] =  tk.Radiobutton(self.__get_blocs()["taille"], text="170-179cm", variable=self.entree_taille, value=Conversion.taille_enum_en_texte(Taille.T170_179),bg="navajo white")
        self.__get_entree()["taille179"].pack()
        self.__get_entree()["taille189"] =  tk.Radiobutton(self.__get_blocs()["taille"], text="180-189cm", variable=self.entree_taille, value=Conversion.taille_enum_en_texte(Taille.T180_189),bg="navajo white")
        self.__get_entree()["taille189"].pack()
        self.__get_entree()["taille199"] =  tk.Radiobutton(self.__get_blocs()["taille"], text="190-199cm", variable=self.entree_taille, value=Conversion.taille_enum_en_texte(Taille.T190_199),bg="navajo white")
        self.__get_entree()["taille199"].pack()
        self.__get_entree()["taille200"] =  tk.Radiobutton(self.__get_blocs()["taille"], text="+200cm", variable=self.entree_taille, value=Conversion.taille_enum_en_texte(Taille.T200_),bg="navajo white")
        self.__get_entree()["taille200"].pack()

        self.__get_blocs()["taille"].pack(side=tk.LEFT)

        # sexe
        self.__get_blocs()["sexe"] = tk.LabelFrame(self.__get_blocs()["taille+sexe+yeux"],text="Sexe", padx=20, pady=5,bg="navajo white")

        self.entree_sexe.set(Conversion.sexe_enum_en_texte(template.get_sexe()))
        self.__get_entree()["homme"] = tk.Radiobutton(self.__get_blocs()["sexe"], text="homme", variable=self.entree_sexe, value=Conversion.sexe_enum_en_texte(Sexe.HOMME), bg="navajo white")
        self.__get_entree()["homme"].pack()
        self.__get_entree()["femme"] = tk.Radiobutton(self.__get_blocs()["sexe"], text="femme", variable=self.entree_sexe, value=Conversion.sexe_enum_en_texte(Sexe.FEMME), bg="navajo white")
        self.__get_entree()["femme"].pack()

        self.__get_blocs()["sexe"].pack()

        # yeux
        self.__get_blocs()["yeux"] = tk.LabelFrame(self.__get_blocs()["taille+sexe+yeux"],text="Couleur des yeux", padx=20, pady=5,bg="navajo white")


        self.entree_yeux.set(Conversion.couleur_yeux_enum_en_texte(template.get_couleur_yeux()))
        self.__get_entree()["yeuxBleu"] =  tk.Radiobutton(self.__get_blocs()["yeux"], text="bleu", variable=self.entree_yeux, value=Conversion.couleur_yeux_enum_en_texte(CouleurYeux.BLEU),bg="navajo white")
        self.__get_entree()["yeuxBleu"].pack()
        self.__get_entree()["yeuxMarron"] =  tk.Radiobutton(self.__get_blocs()["yeux"], text="marron", variable=self.entree_yeux, value=Conversion.couleur_yeux_enum_en_texte(CouleurYeux.MARRON),bg="navajo white")
        self.__get_entree()["yeuxMarron"].pack()
        self.__get_entree()["yeuxVert"] =  tk.Radiobutton(self.__get_blocs()["yeux"], text="vert", variable=self.entree_yeux, value=Conversion.couleur_yeux_enum_en_texte(CouleurYeux.VERT),bg="navajo white")
        self.__get_entree()["yeuxVert"].pack()
        self.__get_entree()["yeuxGris"] =  tk.Radiobutton(self.__get_blocs()["yeux"], text="gris", variable=self.entree_yeux, value=Conversion.couleur_yeux_enum_en_texte(CouleurYeux.GRIS),bg="navajo white")
        self.__get_entree()["yeuxGris"].pack()
        self.__get_entree()["yeuxVairon"] =  tk.Radiobutton(self.__get_blocs()["yeux"], text="vairon", variable=self.entree_yeux, value=Conversion.couleur_yeux_enum_en_texte(CouleurYeux.VAIRON),bg="navajo white")
        self.__get_entree()["yeuxVairon"].pack()

        self.__get_blocs()["yeux"].pack()

        self.__get_blocs()["taille+sexe+yeux"].pack()

        # caractéristiques actuelle
        self.__get_blocs()["autresCarac"] = tk.LabelFrame(self.__get_fenetre_principale(),text="Autres caractéristiques", padx=20, pady=21,bg="navajo white")
        self.__get_champs()["caractéristiques"] = tk.Label(self.__get_blocs()["autresCarac"], text=template.get_caracteristiques_vraies(),wraplength=600,bg="navajo white")
        self.__get_champs()["caractéristiques"].pack()
        self.__get_blocs()["autresCarac"].pack()

        #modifications des caractériqtiques
        self.__get_blocs()["modifCarac"] = tk.Frame(master=self.__get_fenetre_principale(),bg="navajo white")
        # nouvelle caractéristique
        self.liste_type_carac = self.__get_akinator().get_base().liste_type_caracteristique()
        self.nouveaux_types_car = []
        self.__get_blocs()["nouvelleCarac"] = tk.LabelFrame(self.__get_blocs()["modifCarac"],text="Ajouter une caractéristique", padx=20, pady=12,bg="navajo white")
        self.__get_blocs()["listeCaracScroll"] = tk.Frame(master=self.__get_blocs()["nouvelleCarac"],bg="navajo white")

        scrollbar = tk.Scrollbar(self.__get_blocs()["listeCaracScroll"])
        scrollbar.pack(side=tk.RIGHT,fill=tk.Y)

        self.__get_entree()["listeCaracteristiques"] = tk.Listbox(self.__get_blocs()["listeCaracScroll"],selectmode="single")
        for element in self.liste_type_carac:
            self.__get_entree()["listeCaracteristiques"].insert(tk.END, element)
        self.__get_entree()["listeCaracteristiques"].pack()

        self.__get_entree()["listeCaracteristiques"].config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.__get_entree()["listeCaracteristiques"].yview)

        self.__get_blocs()["listeCaracScroll"].pack(side=tk.LEFT)

        self.__get_champs()["valeurCarac"] = tk.Label(self.__get_blocs()["nouvelleCarac"], text="valeur :",bg="navajo white")
        self.__get_champs()["valeurCarac"].pack()
        self.entree_valeur_carac.set("")
        self.__get_entree()["valeurCarac"] = tk.Entry(self.__get_blocs()["nouvelleCarac"], textvariable=self.entree_valeur_carac)
        self.__get_entree()["valeurCarac"].pack()

        self.__get_champs()["verbeCarac"] = tk.Label(self.__get_blocs()["nouvelleCarac"], text="verbe associé :",bg="navajo white")
        self.__get_champs()["verbeCarac"].pack()
        self.entree_verbe_carac.set("")
        self.__get_entree()["verbeCarac"] = tk.Entry(self.__get_blocs()["nouvelleCarac"], textvariable=self.entree_verbe_carac)
        self.__get_entree()["verbeCarac"].pack()

        self.__get_boutons()["ajouterCarac"] = tk.Button(self.__get_blocs()["nouvelleCarac"],bg="burlywood2", fg="brown4")
        self.__get_boutons()["ajouterCarac"]["text"] ="Ajouter"
        self.__get_boutons()["ajouterCarac"]["command"] = lambda : self.nouvelle_carac(template)
        self.__get_boutons()["ajouterCarac"].pack(side=tk.BOTTOM)


        self.__get_blocs()["nouvelleCarac"].pack(side=tk.LEFT)


        # nouveau type de caractéristique
        self.__get_blocs()["nouveauTypeCarac"] = tk.LabelFrame(self.__get_blocs()["modifCarac"],text="Ajouter un type de caractéristique", padx=20, pady=20,bg="navajo white")

        self.__get_champs()["nomCarac"] = tk.Label(self.__get_blocs()["nouveauTypeCarac"], text="Nom de la caractéristique :",bg="navajo white")
        self.__get_champs()["nomCarac"].pack()
        self.entree_nom_carac.set("")
        self.__get_entree()["nomCarac"] = tk.Entry(self.__get_blocs()["nouveauTypeCarac"], textvariable=self.entree_nom_carac)
        self.__get_entree()["nomCarac"].pack()

        self.entree_type_question.set(1)
        self.__get_entree()["typeQuestion1-F"] = tk.Radiobutton(self.__get_blocs()["nouveauTypeCarac"], text="Type 1(féminin)", variable=self.entree_type_question, value=1,bg="navajo white")
        self.__get_entree()["typeQuestion1-F"].pack()
        self.__get_entree()["typeQuestion1-M"] = tk.Radiobutton(self.__get_blocs()["nouveauTypeCarac"], text="Type 1(masculin)", variable=self.entree_type_question, value=4,bg="navajo white")
        self.__get_entree()["typeQuestion1-M"].pack()
        self.__get_entree()["typeQuestion2"] = tk.Radiobutton(self.__get_blocs()["nouveauTypeCarac"], text="Type 2", variable=self.entree_type_question, value=2,bg="navajo white")
        self.__get_entree()["typeQuestion2"].pack()
        self.__get_entree()["typeQuestion3"] = tk.Radiobutton(self.__get_blocs()["nouveauTypeCarac"], text="Type 3", variable=self.entree_type_question, value=3,bg="navajo white")
        self.__get_entree()["typeQuestion3"].pack()

        self.__get_boutons()["ajouterTypeCarac"] = tk.Button(self.__get_blocs()["nouveauTypeCarac"],bg="burlywood2", fg="brown4")
        self.__get_boutons()["ajouterTypeCarac"]["text"] ="Ajouter"
        self.__get_boutons()["ajouterTypeCarac"]["command"] = lambda : self.nouveau_type_carac()
        self.__get_boutons()["ajouterTypeCarac"].pack(side=tk.BOTTOM)

        self.__get_blocs()["nouveauTypeCarac"].pack()


        self.__get_blocs()["modifCarac"].pack()


        # validation
        self.__get_blocs()["boutons-formulaire"] = tk.Frame(master=self.__get_fenetre_principale(),bg="navajo white")
        self.__get_blocs()["boutons-formulaire"].rowconfigure(0, weight=1)
        self.__get_blocs()["boutons-formulaire"].columnconfigure(0, weight=1)
        self.__get_blocs()["boutons-formulaire"].columnconfigure(1, weight=1)
        self.__get_blocs()["boutons-formulaire"].columnconfigure(2, weight=1)
        self.__get_boutons()["enregistrer"] = tk.Button(self.__get_blocs()["boutons-formulaire"],bg="burlywood2", fg="brown4")
        self.__get_boutons()["enregistrer"]["text"] = "Enregistrer"
        self.__get_boutons()["enregistrer"]["command"] = lambda : self.valider_formulaire(template)
        self.__get_boutons()["enregistrer"].pack(side=tk.LEFT)


        self.__get_boutons()["menu"] = tk.Button(self.__get_blocs()["boutons-formulaire"],bg="burlywood2", fg="brown4")
        self.__get_boutons()["menu"]["text"] = "Menu"
        self.__get_boutons()["menu"]["command"] = lambda : self.retour_menu()
        self.__get_boutons()["menu"].pack()

        self.__get_blocs()["boutons-formulaire"].pack(padx=200)

        # on change la police des éléments du formulaire
        for entree in self.__get_entree().values():
            entree["font"] = self.__get_police_formulaire()
            entree.configure(fg="brown4")

        for champ in self.__get_champs().values():
            champ["font"] = self.__get_police_formulaire()
            champ.configure(fg="brown4")

        # on met une police différente pour le titre
        self.__get_champs()["titre"]["font"] = self.__get_police_titre()

        for bouton in self.__get_boutons().values():
            bouton["font"] = self.__get_police_formulaire()
            bouton.configure(fg="brown4")

        for bloc in self.__get_blocs().values():
            if type(bloc) == tk.LabelFrame:
                bloc["font"] = self.__get_police_formulaire()
                bloc.configure(fg="brown4")

        self.__get_fenetre_principale().grid()

    def nouvelle_carac(self,template):
        index_type_carac = self.__get_entree()["listeCaracteristiques"].curselection()
        if len(index_type_carac) != 0 :
            type_carac = self.liste_type_carac[int(index_type_carac[0])]
            carac = Caracteristique(self.entree_valeur_carac.get(),type_carac,self.entree_verbe_carac.get())
            exemple_question = Question(carac)
            message = "La question sera de la forme : \"" + exemple_question.construire_question(Sexe.INCONNU) + "\", cela convient-il ?"
            if tk_box.askokcancel("Nouvelle caractéristique",message) :
                template.get_caracteristiques_vraies().append(carac)
                self.__get_champs()["caractéristiques"]["text"] = template.get_caracteristiques_vraies()
                self.entree_valeur_carac.set("")
                self.entree_verbe_carac.set("")

    def nouveau_type_carac(self):
        if self.entree_type_question.get()!= 4 :
            type_question = self.entree_type_question.get()
            genre_question = Genre.FEMININ
        else :
            type_question = 1
            genre_question = Genre.MASCULIN
        nouveau_type = TypeCaracteristique(self.entree_nom_carac.get(),type_question, genre_question)
        exemple_question = Question(Caracteristique("[valeur]",nouveau_type,"[verbe]"))
        message = "Les questions seront de la forme : \"" + exemple_question.construire_question(Sexe.INCONNU) + "\", cela convient-il ?"
        if tk_box.askokcancel("Nouveaux types de caractéristique",message) :
            self.nouveaux_types_car.append(nouveau_type)
            self.liste_type_carac.append(nouveau_type)
            self.__get_entree()["listeCaracteristiques"].insert(tk.END, nouveau_type)
            self.entree_nom_carac.set("")



    def valider_formulaire(self, template):
        template.set_nom(self.entree_nom.get())
        template.set_est_vrai_nom(self.entree_vrai_nom.get())
        template.set_date_naissance(date(self.entree_annee_nais.get(),self.entree_mois_nais.get(),self.entree_jour_nais.get()))
        template.set_taille(Conversion.taille_texte_en_enum(self.entree_taille.get()))
        template.set_sexe(Conversion.sexe_texte_en_enum(self.entree_sexe.get()))
        template.set_couleur_yeux(Conversion.couleur_yeux_texte_en_enum(self.entree_yeux.get()))
        self.__get_akinator().set_template(template)
        if tk_box.askokcancel("Enregistrer le personnage","Êtes-vous sûr que vous voulez ajouter ce personnage?") :
            if not self.__get_akinator().perso_deja_stocke() :
                self.__get_akinator().get_base().ajouter_perso(template,self.nouveaux_types_car)
                self.afficher_menu(initial=False, victoire=False)
            else :
                tk_box.showerror("Erreur","Impossible d'ajouter le personnage car il est déjà dans la base de données")

    def retour_menu(self) :
        if tk_box.askokcancel("Retour au menu","Retourner au menu sans enregistrer?") :
            self.afficher_menu(initial=False, victoire=False)



    def afficher_manque_info(self):
        self.nettoyer_fenetre()
        self.__fenetre_boutons = tk.Frame(bg="navajo white")
        self.__get_fenetre_boutons().grid(row=0, column=0)

        self.__get_champs()["pasInfos"] = tk.Label(self.__get_fenetre_boutons(), text="Pas assez d'infos")
        self.__get_champs()["pasInfos"].config(bg="navajo white", fg="brown4")
        self.__get_champs()["pasInfos"]["font"] = self.__get_police_titre()
        self.__get_champs()["pasInfos"].grid()

        self.__get_boutons()["retourMenu"] = tk.Button(self.__get_fenetre_boutons())
        self.__get_boutons()["retourMenu"]["text"] = "Retour au menu"
        self.__get_boutons()["retourMenu"]["command"] = lambda : self.afficher_menu(initial=False, victoire=False)
        self.__get_boutons()["retourMenu"].config(bg="burlywood2", fg="brown4")
        self.__get_boutons()["retourMenu"]["font"] = self.__get_police()
        self.__get_boutons()["retourMenu"].grid()
