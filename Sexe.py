from enum import Enum

class Sexe(Enum):
    HOMME = 1
    FEMME = 2
    INCONNU = 3