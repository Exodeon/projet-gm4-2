from TypeCaracteristique import TypeCaracteristique

class Caracteristique:
    def __init__(self, valeur, type_car, verbe):
        self.valeur = valeur
        self.type_caracteristique = type_car
        self.verbe = verbe

    # getteur
    def get_valeur(self):
        return self.valeur

    def get_type_caracteristique(self):
        return self.type_caracteristique

    def get_verbe(self):
        return self.verbe

    def get_nom(self):
        return self.get_type_caracteristique().get_nom()

    # setteur
    def set_valeur(self, valeur):
        self.valeur = valeur

    def set_type_caracteristique(self, type_car):
        self.type_caracteristique = type_car

    def set_verbe(self, verbe):
        self.verbe = verbe

    def set_nom(self, nom):
        self.get_type_caracteristique().set_nom(nom)

    # redéfinition de l'égalité
    def __eq__(self, autre_caracteristique):
        return self.get_nom() == autre_caracteristique.get_nom() and self.get_valeur() == autre_caracteristique.get_valeur()

    # redéfinition de l'affichage
    def __str__(self):
        return self.get_nom() + " => " + str(self.get_valeur())

    def __repr__(self):
        return str(self)
