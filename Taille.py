from enum import Enum

class Taille(Enum):
    T_150 = 1
    T150_159 = 2
    T160_169 = 3
    T170_179 = 4
    T180_189 = 5
    T190_199 = 6
    T200_ = 7
    INCONNUE = 8
