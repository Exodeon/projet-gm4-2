from Sexe import Sexe
from CouleurYeux import CouleurYeux
from Taille import Taille
from datetime import date

class Personnage:
    def __init__(self, nom = "inconnu", date_naissance = "", sexe = Sexe.INCONNU, taille = Taille.INCONNUE, couleur_yeux = CouleurYeux.INCONNU, est_vrai_nom = None):
        self.nom = nom
        self.date_naissance = date_naissance
        self.sexe = sexe
        self.taille = taille
        self.couleur_yeux = couleur_yeux
        self.est_vrai_nom = est_vrai_nom

    # getteur
    def get_nom(self):
        return self.nom

    def get_date_naissance(self):
        return self.date_naissance

    def get_sexe(self):
        return self.sexe

    def get_taille(self):
        return self.taille

    def get_couleur_yeux(self):
        return self.couleur_yeux

    def get_est_vrai_nom(self):
        return self.est_vrai_nom

    # setteur
    def set_nom(self, nom):
        self.nom = nom

    def set_date_naissance(self, date_naissance):
        self.date_naissance = date_naissance

    def set_sexe(self, sexe):
        self.sexe = sexe

    def set_taille(self, taille):
        self.taille = taille

    def set_couleur_yeux(self, couleur_yeux):
        self.couleur_yeux = couleur_yeux

    def set_est_vrai_nom(self, est_vrai_nom):
        self.est_vrai_nom = est_vrai_nom

    # méthodes
    def est_egal(self, perso):
        if (self.nom == perso.nom and self.date_naissance == perso.date_naissance
        and self.sexe == perso.sexe and self.taille == perso.taille
        and self.couleur_yeux == perso.couleur_yeux):
            return True
        else:
            return False
