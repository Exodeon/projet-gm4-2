from Template import Template
from BaseDeDonnees import BaseDeDonnees
from Question import Question
from Interface import Interface
from InterfaceTerminal import InterfaceTerminal
from Menu import Menu
from NoQuestionsException import NoQuestionsException
from Caracteristique import Caracteristique
from TypeCaracteristique import TypeCaracteristique
from Genre import Genre

import random
import numpy
from datetime import date

class Akinator:
    def __init__(self,interface=InterfaceTerminal()):
        self.template = Template()
        self.base = BaseDeDonnees()
        self.questions = []
        self.interface = interface
        self.menu = Menu()

    # getteur
    def get_template(self):
        return self.template

    def get_base(self):
        return self.base

    def get_questions(self):
        return self.questions

    def get_interface(self):
        return self.interface

    def get_menu(self):
        return self.menu

    # setteur
    def set_template(self, template):
        self.template = template

    def set_questions(self, questions):
        self.questions = questions

    # méthodes
    def est_trouve(self):
        # Retourne vrai s'il ne reste plus qu'un personnage ; faux sinon
        if (len(self.get_base().liste_perso(self.get_template())) == 1):
            return True
        else:
            return False

    def perdu(self):
        # Retourne vrai s'il ne reste plus aucun personnage ; faux sinon
        if (len(self.get_base().liste_perso(self.get_template())) == 0):
            return True
        else:
            return False

    def perso_deja_stocke(self):
        # Retourne vrai si le personnage à ajouter est déjà dans la base ; faux sinon
        perso_base = self.get_base().obtenir_perso(self.get_template().get_nom())
        result = False
        for perso in perso_base:
            result = result or perso.est_egal(self.get_template())
        return result

    def trouver_question(self):
        # Méthode permettant de trouver la caractéristique sur laquelle poser la prochaine question
        liste_caracteristiques, nb_perso_restants = self.get_base().caracteristiques_restantes(self.get_template())
        #liste_scores = list(map(lambda element: abs(element[1]/nb_perso_restants - 0.5), liste_caracteristiques))

        #on enlève les caractéristiques qui correspondent à une question déjà posée :
        liste_caracteristiques_question = [question.get_caracteristique() for question in self.get_questions()]
        liste_caracteristiques = [element for element in liste_caracteristiques if element[0] not in liste_caracteristiques_question]

        # pour la caractéristique estVraiNom, on ne veut pas poser 2 fois la question pour True et False
        if "estVraiNom" in [question.get_caracteristique().get_nom() for question in self.get_questions()]:
            liste_caracteristiques = [element for element in liste_caracteristiques if element[0].get_nom() != "estVraiNom"]

        if (len(liste_caracteristiques) == 0):
            raise NoQuestionsException()
        else :
            # on regarde si on pose la question sur la date de naissance, on dit qu'on a 30% de chances de la poser si on ne l'a pas déjà fait
            question_date_deja_posee = "dateNaissance" in [question.get_caracteristique().get_nom() for question in self.get_questions()]
            if not question_date_deja_posee and random.random() <= 0.3:
                # on cherche la date moyenne de toutes les dates restantes
                dates_disponibles = [element[0].get_valeur() for element in liste_caracteristiques if element[0].get_nom() == "dateNaissance"]
                date_moyenne = (numpy.array(dates_disponibles, dtype='datetime64[s]')
                        .view('i8')
                        .mean()
                        .astype('datetime64[s]'))
                # on récupère le 1er janvier de l'année de la date moyenne
                annee = date_moyenne.astype(object).year
                date_question = date(annee, 1, 1)
                type_car = TypeCaracteristique(nom="dateNaissance", type_qu=0, genre=Genre.INCONNU)
                return Question(Caracteristique(valeur=date_question,
                                                type_car=type_car,
                                                verbe=""))

            else:
                # on ne pose pas la question sur la date de naissance, donc on les enlève
                liste_caracteristiques = [element for element in liste_caracteristiques if
                                          element[0].get_nom() != "dateNaissance"]
                if (len(liste_caracteristiques) == 0):
                    raise NoQuestionsException()
                else:
                    minListe = min(liste_caracteristiques, key=lambda element: abs(element[1]/nb_perso_restants - 0.5))[1]
                    caracteristiques_pertinentes = [element for element in liste_caracteristiques if element[1] == minListe]
                    # Pour chaque fréquence de caractéristique, on effectue le traitement suivant :
                    #   On la divise par le nombre de personnages restant pour la normaliser
                    #   On soustrait 0.5 pour centrer en 0
                    #   On applique la valeur absolue
                    #   On cherche alors le minimum, c'est-à-dire la caractéristique dont la fréquence sera la plus proche de la moitié du nb de personnes restantes,
                    #   et donc celle qui permettra d'éliminer le plus de gens possible en posant la question liée à cette caractéristique.

                    # On choisit aléatoirement une caractéristique parmi celles qui sont pertinentes :
                    caracteristique_pertinente = random.choice(caracteristiques_pertinentes)[0]
                    return Question(caracteristique_pertinente)


    def partie(self):
        # Méthode que gère le déroulement d'une partie
        self.reinitialiser()    # Vide le template au début d'une nouvelle partie
        try:
            while not (self.est_trouve() or self.perdu()): # Pose des questions et traite les réponses tant que la partie n'est pas finie
                question = self.trouver_question()
                self.get_questions().append(question)
                question.poser(self.get_template(), self.get_interface())
            if self.est_trouve():   # Si un nom a été trouvé, on demande si le résultat est bon
                nom = self.get_base().obtenir_nom_perso(self.get_base().liste_perso(self.get_template())[0])
                a_enregistrer = self.get_interface().afficher_resultat(nom)
            if (a_enregistrer or self.perdu()):  # Enregistre le nouveau personnage si le joueur le veut
                templ, nouveaux_types_car = self.get_interface().formulaire(self.get_template(),
                                                                            self.get_base().liste_type_caracteristique())
                self.set_template(templ)
                if not self.perso_deja_stocke():
                    self.get_base().ajouter_perso(self.get_template(),
                                                  nouveaux_types_car)
        except NoQuestionsException :
            self.get_interface().afficher_manque_info()


    def reinitialiser(self):
        # Méthode qui réinitialise le template du personnage et la liste des questions déjà posées
        self.set_template(Template())
        self.get_questions().clear()

    def executer(self):
        # Méthode qui gère le déroulement de l'application
        continu = self.get_menu().menu_initial(self.get_interface())
        while continu:
            self.partie()
            continu = self.get_menu().menu_fin_partie(self.get_interface())
