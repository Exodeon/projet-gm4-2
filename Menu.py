from Interface import Interface

class Menu:

    # entrée -> initial : Interface
    # sortie -> Reponse
    def menu_initial(self, interface):
        return interface.afficher_menu(True)

    # entrée -> initial : Interface
    # sortie -> Reponse
    def menu_fin_partie(self, interface):
        return interface.afficher_menu(False)
