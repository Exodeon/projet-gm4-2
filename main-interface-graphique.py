from InterfaceGraphique import InterfaceGraphique
from Template import Template
from Taille import Taille
from Sexe import Sexe
from Caracteristique import Caracteristique
from TypeCaracteristique import TypeCaracteristique
from Genre import Genre
from CouleurYeux import CouleurYeux

inter = InterfaceGraphique()
inter.afficher_menu(initial=True, victoire=False)
inter.mainloop()
