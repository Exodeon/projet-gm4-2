# Projet GM4-2

Projet du deuxième semestre de GM4 - implémentation d'un moteur d'inférence pour recréer une application semblable à Akinator

Pour installer `pymysql` :
```bash
pip3 install pymysql
```
ou, si vous n'avez pas `pip3` :
```bash
python3 -m pip install pymysql
```

On donne les commandes utilisées pour créer les tables de la base de données :
```SQL
CREATE TABLE Personnage(
	idPersonnage INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nomPersonnage VARCHAR (40) NOT NULL
	sexe VARCHAR (8),
	couleurYeux VARCHAR (10),
	taille VARCHAR(7),
	dateNaissance DATE
	);

CREATE TABLE Caracteristique(
	idCaracteristique INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nomCaracteristique VARCHAR (20) NOT NULL,
	idPersonnage INT NOT NULL,
	valeur VARCHAR (20) NOT NULL,
	FOREIGN KEY (idPersonnage) REFERENCES Personnage(idPersonnage)
	);
```
