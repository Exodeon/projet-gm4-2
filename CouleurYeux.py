from enum import Enum

class CouleurYeux(Enum):
    BLEU = 1
    MARRON = 2
    VERT = 3
    GRIS = 4
    VAIRON = 5
    INCONNU = 6
